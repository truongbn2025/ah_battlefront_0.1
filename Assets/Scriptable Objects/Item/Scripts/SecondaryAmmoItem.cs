using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "SecondaryAmmo", menuName = "InventorySystem/Items/Ammo/SecondaryAmmo", order = 1)]
public class SecondaryAmmoItem : ItemObject
{
    
    private void Awake()
    {
        
        type = ITEM_TYPE.AMMO_SECONDARY;
    }
}
