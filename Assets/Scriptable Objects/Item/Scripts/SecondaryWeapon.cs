using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "SecondaryGun", menuName = "InventorySystem/Items/Weapon/SecondaryGun", order = 1)]
public class SecondaryWeapon : ItemObject
{
    public int Amount;
   
    private void Awake()
    {
       
        type = ITEM_TYPE.WEAPON_GUN_SECONDARY;
    }
}
