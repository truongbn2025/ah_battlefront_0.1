using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BodyPartFactory : MonoBehaviour
{
    public static BodyPartFactory Instance;
    private void Awake()
    {
        if(Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
            return;
        }
    }

    public List<Heads> Heads;
    public List<Bodies> Bodies;
    public List<Hips> Hips;
    public List<LeftArms> LeftArms;
    public List<RightArms> RightArms;
    public List<LeftLegs> LeftLegs;
    public List<RightArms> RightLegs;

    public int GetBodyPart(BODY_PART partName, int id)
    {
        //BodyPart bodyPart;
        //switch (partName)
        //{
        //    case BODY_PART.HEAD:
        //        bodyPart = Heads[id];
        //        return bodyPart;
        //    case BODY_PART.BODY:
        //        bodyPart = Bodies[id];
        //        return bodyPart;
        //    case BODY_PART.HIP:
        //        bodyPart = Hips[id];
        //        return bodyPart;
        //    case BODY_PART.LEFT_ARM:
        //        bodyPart = LeftArms[id];
        //        return bodyPart;
        //    case BODY_PART.RIGHT_ARM:
        //        bodyPart = RightArms[id];
        //        return bodyPart;
        //    case BODY_PART.LEFT_LEG:
        //        bodyPart = LeftLegs[id];
        //        return bodyPart;
        //    case BODY_PART.RIGHT_LEG:
        //        bodyPart = RightLegs[id];
        //        return bodyPart;
        //}

        return 0;
    }

}
