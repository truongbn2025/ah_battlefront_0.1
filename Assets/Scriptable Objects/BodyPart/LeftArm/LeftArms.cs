
using UnityEngine;

[CreateAssetMenu(fileName = "LeftArm", menuName = "BodyPartSystem/LeftArm", order = 1)]

public class LeftArms : BodyPart
{

    private void Awake()
    {
        partName = BODY_PART.LEFT_ARM;
    }
}
