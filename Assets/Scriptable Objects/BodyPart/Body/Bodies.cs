using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Body", menuName = "BodyPartSystem/Body", order = 1)]

public class Bodies : BodyPart
{
    private void Awake()
    {
        partName = BODY_PART.BODY;
    }
}
