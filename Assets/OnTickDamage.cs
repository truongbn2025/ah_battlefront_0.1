using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OnTickDamage : MonoBehaviour
{

    public float onTickDamage = 0.2f;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("ToxicArea"))
        {
            GetComponent<Health>().SetBurnState(true, onTickDamage);
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("ToxicArea"))
        {
            GetComponent<Health>().SetBurnState(false, onTickDamage);
        }
    }
}
