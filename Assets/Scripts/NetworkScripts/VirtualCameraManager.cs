using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VirtualCameraManager : MonoBehaviour
{
    public GameObject normalCamera;
    public GameObject aimCamera;
    public GameObject dropShipCamera;
    public GameObject mainCamera;

}
