using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
public class SliderScript : MonoBehaviour
{
    [SerializeField] Slider slider;
    [SerializeField] TextMeshProUGUI maxAmount;
    [SerializeField] TMP_InputField amount;

    private void Start()
    {
        slider.onValueChanged.AddListener((value) => {
            amount.text = (Int32.Parse(maxAmount.text) * value).ToString("0");

        } );
    }

    private void OnEnable()
    {
        amount.text = "0";
        slider.value = 0;
    }

}
