using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class KillFeedManager : MonoBehaviour
{
    public int KillBannerLimit = 5;
    public float ShowTimer = 3f;
    public bool changeColorForAllies = true;
    public Color MyKillColor;
    public Color AlliesKillColor;
    public Color DefautKillColor;

    [SerializeField]
    private GameObject KillMessage;

    private List<GameObject> KillMessages = new List<GameObject>();

    public void CreateBanner(string killer, string victim, string weapon, bool isMyKill, bool isAlliesKill)
    {
        GameObject banner = Instantiate(KillMessage, gameObject.transform);
        KillMessages.Add(banner);

        var bannerText = banner.GetComponent<TextMeshProUGUI>();
        bannerText.text = killer + " killed " + victim + " with " + weapon;

        bannerText.color = DefautKillColor;
        bannerText.color = isAlliesKill ? AlliesKillColor : bannerText.color;
        bannerText.color = isMyKill ? MyKillColor : bannerText.color;

        banner.GetComponent<AutoDestroyOffline>().time = ShowTimer;
    }

    private void Update()
    {
        if (KillMessages.Count > KillBannerLimit)
            KillMessages.RemoveAt(0);
    }
}
