using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class FPSCounter : MonoBehaviour
{
    [SerializeField] TextMeshProUGUI fps;

    private float pollingTime = 1f;
    private float time;
    private float frameCount;

    private void Update()
    {
        time += Time.deltaTime;

        frameCount++;

        if(time >= pollingTime)
        {
            int framerate = Mathf.RoundToInt(frameCount / time);
            fps.text = framerate.ToString() + " FPS";

            time -= pollingTime;
            frameCount = 0;
        }
    }
}
