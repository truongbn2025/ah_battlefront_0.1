using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;
public class InputFieldScript : MonoBehaviour
{
    [SerializeField] Slider slider;
    [SerializeField] TextMeshProUGUI maxAmount;
    [SerializeField] TMP_InputField amount;

    private void Start()
    {
        amount.onEndEdit.AddListener((value) =>
        {
            float _amount = Int32.Parse(amount.text);
            float max = Int32.Parse(maxAmount.text);
            if (_amount <= 0)
            {
                _amount = 0;
            }else if(_amount>= max)
            {
                _amount = max;
            }
            else
            {
                _amount = Int32.Parse(amount.text);
                
            }
            Debug.Log(_amount / max);
            slider.value = (_amount / max);
        });
    }
}
