using FishNet.Object;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class NewUIManager : NetworkBehaviour
{
    [SerializeField] Health health;
    [SerializeField] private TextMeshProUGUI healthText;
    [SerializeField] private Image heathBar;
    [SerializeField] private PlayerStatsManager playerStatsManager;

    public override void OnStartClient()
    {
        base.OnStartClient();
        if (!IsOwner) gameObject.SetActive(false);
    }

    private void OnEnable()
    {
        health.HealthUpdate += OnHealthChanged;
    }

    private void OnDisable()
    {
        health.HealthUpdate -= OnHealthChanged;
    }

    public void OnHealthChanged(float health)
    {
        var maxHealth = playerStatsManager.GetHealth();
        healthText.text = health.ToString("0") + "/" + maxHealth;
        heathBar.fillAmount = health / maxHealth;
    }
}
