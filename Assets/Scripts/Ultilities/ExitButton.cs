using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
public class ExitButton : MonoBehaviour, IPointerUpHandler
{
    [SerializeField] GameObject target;
    public void OnPointerUp(PointerEventData eventData)
    {
        target.SetActive(false);
    }
}
