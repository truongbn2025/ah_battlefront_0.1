using FishNet.Component.Animating;
using FishNet.Connection;
using FishNet.Object;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleWeapon : NetworkBehaviour
{
    [SerializeField]
    private List<GameObject> weaponsBack;

    [SerializeField]
    private List<GameObject> weaponsHand;

    [SerializeField]
    private Transform meleBoxcastCenter;

    [SerializeField]
    private LayerMask meleeHitBoxLayer;

    [SerializeField]
    private GameObject hitEffect;

    [SerializeField]
    private Transform weaponBlade;

    private InputManager inputManager;
    private Animator animator;
    private NetworkAnimator networkAnimator;
    private PlayerStates playerStates;
    

    private int currentWeapon = 0;

    public bool meleeEquiped = true;
    private bool canAttack = true;


    private void Awake()
    {
        inputManager = FindObjectOfType<InputManager>();
        animator = GetComponent<Animator>();
        networkAnimator = GetComponent<NetworkAnimator>();
        playerStates = GetComponent<PlayerStates>();

    }
    public override void OnStartClient()
    {
        base.OnStartClient();

        TimeManager.OnTick += OnTick;
    }


    private void OnTick()
    {

    }

    private void Update()
    {
        if (!base.IsOwner) return;

        if(canAttack && inputManager.mainAction && meleeEquiped && playerStates.CanAttack())
        {
            CallAttack();
        }
    }

    public void FinishAttack()
    {
        UnequipWeapon();

        if (base.IsServer)
            ResetAttack(Owner);
    }

    public void Hit()
    {
        if (!base.IsServer) return;

        Collider[] hit = Physics.OverlapBox(meleBoxcastCenter.position, new Vector3(0.75f, 0.5f, 0.4f), meleBoxcastCenter.rotation, meleeHitBoxLayer, QueryTriggerInteraction.Collide);

        
        foreach(var colider in hit)
        {
            if (colider.GetComponentInParent<MeleWeapon>().Owner != Owner)
            {
                colider.GetComponentInParent<Health>().ReduceHealth(20, GetComponent<NetworkCharacter>(), "Neon Blade");
                Debug.Log("hit");
                Instantiate(hitEffect, colider.GetComponentInParent<MeleWeapon>().weaponBlade.position, Quaternion.identity, null);
            }
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawWireCube(meleBoxcastCenter.position, new Vector3(1f, 1, 0.8f));
    }

    [ServerRpc(RunLocally = true)]
    private void CallAttack()
    {
        networkAnimator.SetTrigger("Attack");
        animator.SetInteger("AttackIndex", Random.Range(0, 3));
        animator.SetFloat("Speed", 0);
        canAttack = false;
        playerStates.state = State.Attacking;
        EquipWeapon();
    }

    [TargetRpc(RunLocally = true)]
    private void ResetAttack(NetworkConnection conn)
    {
        canAttack = true;
        playerStates.state = State.Normal;
    }


    //RPCS
    [ServerRpc(RunLocally = true)]
    public void EquipWeapon()
    {
        weaponsBack[currentWeapon].SetActive(false);
        weaponsHand[currentWeapon].SetActive(true);
        EquipWeapon_Observer();
    }

    [ServerRpc(RunLocally = true)]
    public void UnequipWeapon()
    {
        weaponsBack[currentWeapon].SetActive(true);
        weaponsHand[currentWeapon].SetActive(false);
        UnequipWeapon_Observer();
    }


    [ObserversRpc]
    public void EquipWeapon_Observer()
    {
        if (IsOwner) return;
        weaponsBack[currentWeapon].SetActive(false);
        weaponsHand[currentWeapon].SetActive(true);
    }

    [ObserversRpc]
    public void UnequipWeapon_Observer()
    {
        if (IsOwner) return;
        weaponsBack[currentWeapon].SetActive(true);
        weaponsHand[currentWeapon].SetActive(false);
    }

}
