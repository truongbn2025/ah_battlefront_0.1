using System.Collections;
using UnityEngine;
using FishNet.Object;

public class LootItem : NetworkBehaviour
{
    private float speed = 0.05f;
    [ObserversRpc (RunLocally =true)]
    public void StartMoving(Vector3 destination)
    {
        StartCoroutine(Move(destination));
    }
    IEnumerator Move(Vector3 destination)
    {
        while (Vector3.Distance(transform.position, destination) > speed)
        {
            transform.position = Vector3.MoveTowards(transform.position, destination, speed);
            yield return new WaitForEndOfFrame();
        }
        transform.position = destination;
    }
  
}
