using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FishNet;
using FishNet.Object;

public class SoundEffectPlayer : NetworkBehaviour
{
    public static SoundEffectPlayer Instance;

    public List<AudioClip> SoundEffectList = new List<AudioClip>();

    private void Awake()
    {
        if (Instance == null)
            Instance = this;
        else
            Destroy(gameObject);
    }

    [ObserversRpc]
    public void ClientsPlaySoundAtPoint(int audioClipID, Vector3 position)
    {
        if (base.IsOwner) return;
        PlaySoundAtPoint(SoundEffectList[audioClipID], position);
    }

    [ServerRpc(RunLocally = true)]
    private void RequestPlaySoundAtPoint(int audioClipID, Vector3 position)
    {
        PlaySoundAtPoint(SoundEffectList[audioClipID], position);
        ClientsPlaySoundAtPoint(audioClipID, position);
    }

    private void PlaySoundAtPoint(AudioClip audioClip, Vector3 position)
    {
        //Play
    }

}
