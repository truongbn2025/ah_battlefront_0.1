using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DummyCharacterGenerator : MonoBehaviour
{
    [SerializeField] List<GameObject> Heads;
    [SerializeField] List<GameObject> Bodys;
    [SerializeField] List<GameObject> Thights;
    [SerializeField] List<GameObject> LArms;
    [SerializeField] List<GameObject> RArms;
    [SerializeField] List<GameObject> LLegs;
    [SerializeField] List<GameObject> RLegs;

    public PartID partID = new PartID();

    private void Awake()
    {
        GeneratePartDummy();
        DisableAllPart();
        LoadBodyParts(partID);
    }

    private void DisableAllPart()
    {
        foreach (var skin in Heads)
            skin.SetActive(false);
        foreach (var skin in Bodys)
            skin.SetActive(false);
        foreach (var skin in Thights)
            skin.SetActive(false);
        foreach (var skin in LArms)
            skin.SetActive(false);
        foreach (var skin in RArms)
            skin.SetActive(false);
        foreach (var skin in LLegs)
            skin.SetActive(false);
        foreach (var skin in RLegs)
            skin.SetActive(false);
    }

    private void GeneratePartDummy()
    {
        PartID newPart = new PartID();
        newPart.Head = UnityEngine.Random.Range(0, 4);
        newPart.Body = UnityEngine.Random.Range(0, 4);
        newPart.Thight = UnityEngine.Random.Range(0, 4);
        newPart.LArm = UnityEngine.Random.Range(0, 4);
        newPart.RArm = 1;
        newPart.LLeg = UnityEngine.Random.Range(0, 4);
        newPart.RLeg = UnityEngine.Random.Range(0, 4);

        partID = newPart;
    }

    private void LoadBodyParts(PartID partID)
    {
        DisableAllPart();

        Heads[partID.Head].SetActive(true);
        Bodys[partID.Body].SetActive(true);
        Thights[partID.Thight].SetActive(true);
        LArms[partID.LArm].SetActive(true);
        RArms[partID.RArm].SetActive(true);
        LLegs[partID.LLeg].SetActive(true);
        RLegs[partID.RLeg].SetActive(true);

        
    }
}
