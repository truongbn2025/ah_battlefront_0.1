using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestGrenadeDummy : MonoBehaviour
{

    [Range(0.0f, 1.0f)]
    public float velocityLossOnCollision = 0.5f;

    [Range(0.0f, 20f)]
    public float explosionDelay = 2f;

    [Range(0.0f, 20f)]
    public float explosionSize = 2f;

    [SerializeField]
    private float damage = 70f;

    [SerializeField]
    private float explodeVelocity = 8f;

    [SerializeField]
    private ParticleSystem explodeEffect;

    [SerializeField]
    private AudioClip explodeSound;

    private bool exploding = false;
    private bool ignorePlayer = true;

    public void ApplyThrowImpulse(Vector3 force, Vector3 angulatVelocity)
    {
        GetComponent<Rigidbody>().AddForce(force);
        GetComponent<Rigidbody>().angularVelocity = angulatVelocity;
        Invoke("ActivatePlayerCollision", 0.3f);
    }


    private void OnCollisionEnter(Collision collision)
    {

        if (collision.collider.CompareTag("Player") && ignorePlayer) return;


        GetComponent<Rigidbody>().velocity = GetComponent<Rigidbody>().velocity * velocityLossOnCollision;

        if (!exploding)
        {
            Invoke("Explode", 2);
            exploding = true;
        }
    }

    private void ActivatePlayerCollision()
    {
        ignorePlayer = false;
    }

    private void Explode()
    {
        //ExplodeEffect(transform.position);
        Destroy(gameObject);
    }

    private void ExplodeEffect(Vector3 position)
    {
        var explosion = Instantiate(explodeEffect, position, Quaternion.identity);
        explosion.transform.localScale = Vector3.one * explosionSize;

        AudioSource.PlayClipAtPoint(explodeSound, position);
    }
}
