using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
public class ShowTooltip : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler
{
    private ItemObject _item;
    private RectTransform _rectTransform;
    
    public void OnPointerEnter(PointerEventData eventData)
    {
        if(_item != null)
        {
            ChangePivot();
            //ToolTip.Instance.tooltipAvailable = true;

            ToolTip.Instance.ChangeTooltipsProperties(_item.name, ItemTypeToString(_item.type), _item.description, _item.rarity, _item.tooltipIcon);
            ToolTip.Instance.ShowToolTip(transform.position, CheckPosition());
        }
        
    }
    public void OnPointerExit(PointerEventData eventData)
    {
        
        ToolTip.Instance.HideTooltip();    
        //ToolTip.Instance.tooltipAvailable = false;
    }
    private string ItemTypeToString(ITEM_TYPE type)
    {
        switch (type)
        {
            case ITEM_TYPE.WEAPON_GUN_PRIMARY:
                return "Primary Weapon";
            case ITEM_TYPE.WEAPON_GUN_SECONDARY:
                return "Special Weapon";
            case ITEM_TYPE.WEAPON_MELEE:
                return "Melee Weapon";
            case ITEM_TYPE.HEALING:
                return "Healing Item";
            case ITEM_TYPE.GRENADE:
                return "Granade";
            case ITEM_TYPE.CURRENCY:
                return "Currency";

        }
        return "null";
    }
    private void ChangePivot()
    {
        switch (CheckPosition())
        {
            case SCREEN_POSITION.BOTTOM_LEFT:
                _rectTransform.pivot = new Vector2(1, 1);
                break;
            case SCREEN_POSITION.TOP_LEFT:
                _rectTransform.pivot = new Vector2(1, 0);
                break;
            case SCREEN_POSITION.BOTTOM_RIGHT:
                _rectTransform.pivot = new Vector2(0, 1);
                break;
            case SCREEN_POSITION.TOP_RIGHT:
                _rectTransform.pivot = new Vector2(0, 0);
                break;
        }
    }
    SCREEN_POSITION CheckPosition()
    {
        if ((transform.position.y) > Screen.height / 2.0f && transform.position.x > Screen.width / 2.0f)
            return SCREEN_POSITION.TOP_RIGHT;
        if (!(transform.position.y > Screen.height / 2.0f) && transform.position.x > Screen.width / 2.0f)
            return SCREEN_POSITION.BOTTOM_RIGHT;
        if ((transform.position.y) > Screen.height / 2.0f && !(transform.position.x > Screen.width / 2.0f))
            return SCREEN_POSITION.TOP_LEFT;
        return SCREEN_POSITION.BOTTOM_LEFT;
    }
    public void OnPointerDown(PointerEventData eventData)
    {
        ToolTip.Instance.HideTooltip();
        //ToolTip.Instance.tooltipAvailable = false;
    }

    #region MonoBehavior
    private void Awake()
    {
        _rectTransform = GetComponent<RectTransform>();
        _item = GetComponent<InventoryItem>()._item;

    }
    #endregion
}
