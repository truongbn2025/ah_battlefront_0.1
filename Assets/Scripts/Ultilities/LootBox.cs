using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FishNet.Object;

public class LootBox : NetworkBehaviour
{
    [SerializeField] private List<GameObject> lootPrefabs = new List<GameObject>();
    [SerializeField] private List<Transform> destinations = new List<Transform>();
    [SerializeField] private GameObject openEffect;
    private Transform spawnLocation;
    private List<GameObject> _spawnItem = new List<GameObject>();
    private Collider _collider;
    private bool isInteracted = false;

    
    IEnumerator CreateLoot()
    {
        yield return new WaitForSeconds(0.45f);
        SpawnLoot();
    }
    private void SpawnLoot()
    {
        //Classify scenarios
        switch (Random.Range(1, 5))
        {
            case 1:
                //Spawn Range weapon + ammo
                //Add item to list spawn
                GameObject item;
                _spawnItem.Add(lootPrefabs[Random.Range(0, 2)]);
                int ammountSpawn = Random.Range(1, 3);
                for (int i = 0; i < ammountSpawn; i++)
                {
                    _spawnItem.Add(lootPrefabs[6]);
                }
                for (int i = 0; i < _spawnItem.Count; i++)
                {
                    item = Instantiate(_spawnItem[i], spawnLocation.position, transform.rotation, null);
                    Spawn(item);
                    item.GetComponent<LootItem>().StartMoving(destinations[i].position);
                }
                break;
            case 2:
                //Spawn Secondary weapon + ammo
                _spawnItem.Add(lootPrefabs[2]);
                ammountSpawn = Random.Range(1, 3);
                for (int i = 0; i < ammountSpawn; i++)
                {
                    _spawnItem.Add(lootPrefabs[7]);
                }
                for (int i = 0; i < _spawnItem.Count; i++)
                {
                    item = Instantiate(_spawnItem[i], spawnLocation.position, transform.rotation, null);
                    Spawn(item);
                    item.GetComponent<LootItem>().StartMoving(destinations[i].position);
                }
                break;
            case 3:
                //Spawn Ammo with Heal or Grenade
                ammountSpawn = Random.Range(1, 5);
                for (int i = 0; i < ammountSpawn; i++)
                {
                    _spawnItem.Add(lootPrefabs[Random.Range(4, 8)]);
                }
                for (int i = 0; i < _spawnItem.Count; i++)
                {
                    item = Instantiate(_spawnItem[i], spawnLocation.position, transform.rotation, null);
                    Spawn(item);
                    item.GetComponent<LootItem>().StartMoving(destinations[i].position);
                }


                break;
            case 4:
                //Spawn Melee with Heal or Grenade
                _spawnItem.Add(lootPrefabs[3]);
                ammountSpawn = Random.Range(1, 3);
                for (int i = 0; i < ammountSpawn; i++)
                {
                    _spawnItem.Add(lootPrefabs[Random.Range(4, 6)]);
                }

                for (int i = 0; i < _spawnItem.Count; i++)
                {
                    item = Instantiate(_spawnItem[i], spawnLocation.position, transform.rotation, null);
                    Spawn(item);
                    item.GetComponent<LootItem>().StartMoving(destinations[i].position);
                }
                break;
        }
    }

    #region MonoBehavior
    private void Start()
    {
        spawnLocation = transform.GetChild(3).transform;
        _collider = GetComponent<Collider>();
    }
    #endregion

    #region RPCs
    [ServerRpc(RequireOwnership = false)]
    public void RequestOpen()
    {
        if (isInteracted == false)
        {
            // int numberItemSpawn = Random.Range(minItemSpawn, maxItemSpawn);
            OpenBoxObserver();
            StartCoroutine(CreateLoot());
            isInteracted = true;
        }
    }

    [ObserversRpc(RunLocally = true)]
    private void OpenBoxObserver()
    {
        openEffect.SetActive(true);
        GetComponentInChildren<Animator>().SetTrigger("Open");
        _collider.enabled = false;
    }
    #endregion
   
}
