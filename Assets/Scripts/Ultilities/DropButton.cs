using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using System;
public class DropButton : MonoBehaviour, IPointerUpHandler
{
    [SerializeField] PlayerInventory playerInventory;
    [SerializeField] GameObject dropItemUI;
    public void OnPointerUp(PointerEventData eventData)
    {
        
        dropItemUI.SetActive(false);
        if(dropItemUI.GetComponent<DropItemUI>().item != null)
        {
            ItemObject item = dropItemUI.GetComponent<DropItemUI>().item;
            int amount = Int32.Parse(dropItemUI.GetComponent<DropItemUI>().amount.text);
            Debug.Log("drop: " + item.name + " amount: " + amount);
            playerInventory.DropItem(item, amount);
        }
        
    }


}
