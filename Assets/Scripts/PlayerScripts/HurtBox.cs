using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HurtBox : MonoBehaviour
{
    public float Multiplier;
    public event Action<float, NetworkCharacter, string> hit;


    public void Hit(float weaponDamage, NetworkCharacter source, string weaponName)
    {
        hit?.Invoke(Multiplier * weaponDamage, source, weaponName);
    }
}
