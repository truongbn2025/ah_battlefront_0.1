using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStatsManager : MonoBehaviour
{
    public int Strength { get; private set; }
    public int Agility { get; private set; }
    public int Intelligent { get; private set; }

    private void Awake()
    {
        GetTheStats();
    }
    private void GetTheStats()
    {
        Strength = Random.Range(80, 120);
        Agility = Random.Range(80, 120);
        Intelligent = Random.Range(80, 120);
    }

    public int GetHealth()
    {
        return Strength;
    }

    public float GetSpeed()
    {
        return 4 * (Agility / 100f);
    }

    public float GetResistance()
    {
        return 2 * Strength / 100f;
    }
}
