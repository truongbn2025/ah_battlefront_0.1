using FishNet;
using FishNet.Connection;
using FishNet.Object;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClientPlayerSpawner : NetworkBehaviour
{
    [Tooltip("Character prefab to spawn.")]
    [SerializeField]
    private GameObject _characterPrefab;

    [SerializeField]
    private GameObject _clientCanvas;

    public GameObject LocalPlayer;

    public override void OnStartClient()
    {
        base.OnStartClient();
        if (base.IsOwner)
        {
            CmdSpawnPlayer(SpawnPoint.Instance.GetRandomSpawnPoint());
            FindObjectOfType<MatchManager>().LocalInstance = gameObject;
        }
        else
        {
            _clientCanvas.SetActive(false);
        }
    }

    [ServerRpc]
    public void CmdSpawnPlayer(Vector3 spawnPoint)
    {
        SpawnPlayer(spawnPoint, true);
    }

    private void Update()
    {
        if (!base.IsServer) return;
        if (LocalPlayer != null)
            transform.position = LocalPlayer.transform.position;

    }

    public void SpawnPlayer(Vector3 spawnPoint, bool isInvincible)
    {
        if (LocalPlayer != null) return;
        GameObject player = Instantiate(_characterPrefab, spawnPoint, Quaternion.identity);
        base.Spawn(player, base.Owner);
        LocalPlayer = player;
        LocalPlayer.GetComponent<Health>().Die += OnDie;
        LocalPlayer.GetComponent<Health>().isInvincible = isInvincible;
        OnSpawnClient(player);
    }

    private void OnDie(NetworkCharacter killer, string weaponName)
    {
        if (!base.IsServer) return;

        FindObjectOfType<MatchManager>().OnPlayerDie(killer, LocalPlayer.GetComponent<NetworkCharacter>(), weaponName);
        OnDieClient();
    }

    [ObserversRpc]
    private void OnSpawnClient(GameObject localPlayer)
    {
        if(base.IsOwner)
        {
            _clientCanvas.SetActive(false);
            LocalPlayer = localPlayer;
        }
    }

    [ObserversRpc]
    private void OnDieClient()
    {
        if(base.IsOwner)
            _clientCanvas.SetActive(true);
    }

    [ServerRpc]
    public void Respawn(bool isInvincible)
    {
        InstanceFinder.ServerManager.Despawn(LocalPlayer);
        LocalPlayer = null;
        SpawnPlayer(SpawnPoint.Instance.GetRandomSpawnPoint(), isInvincible);
    }

    [ServerRpc]
    public void Despawn()
    {
        InstanceFinder.ServerManager.Despawn(LocalPlayer);
        LocalPlayer = null;
    }

    [ServerRpc]
    public void DropPlayer(GameObject player)
    {
        player.transform.position = FindObjectOfType<DropShipController>().PlayerDropPoint.transform.position;
        player.SetActive(true);
        player.GetComponent<Health>().isInvincible = false;
        //player.GetComponent<Health>().isInvincible = FindObjectOfType<MatchManager>().AlivePlayers == 1;
        player.GetComponent<NetworkCharacter>().characterState = CharacterState.Droping;
        player.GetComponent<NetworkCharacter>().canEverSprint = false;
        DropPlayerClient(player);
    }

    [ObserversRpc]
    public void DropPlayerClient(GameObject player)
    {
        player.transform.position = FindObjectOfType<DropShipController>().PlayerDropPoint.transform.position;
        player.SetActive(true);
    }
}
