using UnityEngine;
using FishNet.Object;
using FishNet.Connection;
using TMPro;
using UnityEngine.UI;
using System;

public class PlayerInventory : NetworkBehaviour
{
    [Header("Inventory")]
    //public List<InventoryObject> inventoryObjects = new List<InventoryObject>();
    //public List<SurroundingObject> surroundingObjects = new List<SurroundingObject>();
    [SerializeField] private InventoryObject inventory;
    [SerializeField] private Transform DropPosition;

   
    [Header("Starter Items")]
    [SerializeField] private ItemObject PrimaryAmmoObject;
    [SerializeField] private ItemObject SecondaryAmmoObject;

    [Header("Inventory Components")]
    [SerializeField] private GameObject invPanel;
    [SerializeField] private Transform invObjectHolder;
    [SerializeField] private Transform primaryGunHolder;
    [SerializeField] private Transform secondaryGunHolder;
    [SerializeField] private Transform meleeHolder;
    [SerializeField] private Transform grenadeHolder;
    [SerializeField] private GameObject primaryAmmoHolder;
    [SerializeField] private GameObject secondaryAmmoHolder;

    [Header("Component Display In Inventory")]
    [SerializeField] private GameObject invCanvasObject;

    [Header("Drop Canvas")]
    [SerializeField] private GameObject dropCanvas;
    [SerializeField] private TextMeshProUGUI playerName;
    [SerializeField] private TextMeshProUGUI strenght;
    [SerializeField] private TextMeshProUGUI agility;
    [SerializeField] private TextMeshProUGUI intel;

    //[Header("Pickup")]
    //[SerializeField] LayerMask pickupLayer;
    //[SerializeField] float pickupDistance;
    //[SerializeField] KeyCode pickupButton = KeyCode.E;

    private Camera cam;
    private Transform worldObjectHolder;
    private NetworkCharacter networkCharacter;
    private WeaponsController weaponsController;

    public void Pickup(RaycastHit hit)
    {
        if (networkCharacter.characterState != CharacterState.Playing || !networkCharacter.characterMovement.isGrounded || weaponsController.combatState != CombatState.Normal)
            return;

        var item = hit.transform.GetComponent<GroundItem>();
        if (item)
        {
            RequestPickup(this.transform, hit.transform, base.Owner);
        }
    }

    public void ClassifyInInventory(ItemObject item, int amount)
    {
        switch (item.type)
        {
            case ITEM_TYPE.WEAPON_MELEE:
                for (int i = 0; i < inventory.MeleeWeapon.Count; i++)
                {
                    ItemObject tempItem = inventory.MeleeWeapon[i].item;
                    DropItem(tempItem);
                }

                
                inventory.AddMeleeWeapon(item);
                if (IsServer)
                    Actions.OnWeaponSwitch(WEAPON_TYPE.MELEE, item.weaponIndex);
                break;
            case ITEM_TYPE.WEAPON_GUN_PRIMARY:
                for (int i = 0; i < inventory.PrimaryGun.Count; i++)
                {
                    ItemObject tempItem = inventory.PrimaryGun[i].item;
                    DropItem(tempItem);
                }
                inventory.AddPrimaryGun(item);
                if (IsServer)
                    Actions.OnWeaponSwitch(WEAPON_TYPE.MAIN_GUN, item.weaponIndex);
                break;
            case ITEM_TYPE.WEAPON_GUN_SECONDARY:
                for (int i = 0; i < inventory.SecondaryGun.Count; i++)
                {
                    ItemObject tempItem = inventory.SecondaryGun[i].item;
                    DropItem(tempItem);


                }
                inventory.AddSecondaryGun(item);
                if (IsServer)
                    Actions.OnWeaponSwitch(WEAPON_TYPE.SIZE_ARM, item.weaponIndex);
                break;
            case ITEM_TYPE.AMMO_PRIMARY:
                inventory.AddPrimaryAmmo(item, amount);
                Actions.OnPrimaryAmmoChange?.Invoke(inventory.PrimaryAmmo[0].amount);
                Debug.Log(inventory.PrimaryAmmo[0].amount);
                break;
            case ITEM_TYPE.AMMO_SECONDARY:

                inventory.AddSecondaryAmmo(item, amount);
                Actions.OnSecondaryAmmoChange?.Invoke(inventory.SecondaryAmmo[0].amount);
                break;
            case ITEM_TYPE.HEALING:
                inventory.AddItem(item, 1);
                break;
            case ITEM_TYPE.CURRENCY:
                inventory.AddItem(item, 1);
                break;
            case ITEM_TYPE.GRENADE:
                inventory.AddGrenade(item, 1);
                Actions.OnGrenadeNumberChange?.Invoke(inventory.Grenade[0].amount);
                break;
        }
    }

    public void ToggleInventory()
    {
        if (invPanel.activeSelf)
        {
            dropCanvas.SetActive(false);
            weaponsController.SetCanAimFlag(true);
            ToolTip.Instance.HideTooltip();
            invPanel.SetActive(false);

            Cursor.lockState = CursorLockMode.Locked;
            Cursor.visible = false;
        }
        else if (!invPanel.activeSelf)
        {
            weaponsController.SetCanAimFlag(false);

            UpdateInvUI();

            invPanel.SetActive(true);

            Cursor.lockState = CursorLockMode.None;
            Cursor.visible = true;
        }
    }

    private void UpdateInvUI()
    {
        foreach (Transform child in invObjectHolder)
            Destroy(child.gameObject);
        foreach (Transform child in meleeHolder)
            Destroy(child.gameObject);
        foreach (Transform child in primaryGunHolder)
            Destroy(child.gameObject);
        foreach (Transform child in secondaryGunHolder)
            Destroy(child.gameObject);
        foreach (Transform child in grenadeHolder)
            Destroy(child.gameObject);


        for (int i = 0; i < inventory.Container.Count; i++)
        {
            GameObject obj = Instantiate(invCanvasObject, invObjectHolder);
            obj.GetComponent<InventoryItem>()._item = inventory.Container[i].item;
            obj.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = inventory.Container[i].amount.ToString();
            obj.transform.GetComponent<Button>().image.sprite = inventory.Container[i].item.icon;
            ItemObject tempItem = inventory.Container[i].item;
            obj.GetComponent<Button>().onClick.AddListener(delegate { DropItem(tempItem); });


        }
        for (int i = 0; i < inventory.MeleeWeapon.Count; i++)
        {
            GameObject obj = Instantiate(invCanvasObject, meleeHolder);
            obj.GetComponent<InventoryItem>()._item = inventory.MeleeWeapon[i].item;
            ItemObject tempItem = inventory.MeleeWeapon[0].item;
            obj.transform.GetComponent<Button>().image.sprite = inventory.MeleeWeapon[i].item.icon;

            obj.GetComponent<Button>().onClick.AddListener(delegate { DropItem(tempItem); });
        }
        for (int i = 0; i < inventory.PrimaryGun.Count; i++)
        {
            GameObject obj = Instantiate(invCanvasObject, primaryGunHolder);
            obj.GetComponent<InventoryItem>()._item = inventory.PrimaryGun[i].item;
            ItemObject tempItem = inventory.PrimaryGun[0].item;
            obj.transform.GetComponent<Button>().image.sprite = inventory.PrimaryGun[i].item.icon;

            obj.GetComponent<Button>().onClick.AddListener(delegate { DropItem(tempItem); });
        }
        for (int i = 0; i < inventory.SecondaryGun.Count; i++)
        {
            GameObject obj = Instantiate(invCanvasObject, secondaryGunHolder);
            obj.GetComponent<InventoryItem>()._item = inventory.SecondaryGun[i].item;
            ItemObject tempItem = inventory.SecondaryGun[0].item;
            obj.transform.GetComponent<Button>().image.sprite = inventory.SecondaryGun[i].item.icon;

            obj.GetComponent<Button>().onClick.AddListener(delegate { DropItem(tempItem); });
        }
        for (int i = 0; i < inventory.Grenade.Count; i++)
        {
            GameObject obj = Instantiate(invCanvasObject, grenadeHolder);
            obj.GetComponent<InventoryItem>()._item = inventory.Grenade[i].item;
            obj.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = inventory.Grenade[i].amount.ToString();
            obj.transform.GetComponent<Button>().image.sprite = inventory.Grenade[i].item.icon;
            ItemObject tempItem = inventory.Grenade[0].item;
            obj.GetComponent<Button>().onClick.AddListener(delegate { DropItem(tempItem); });
        }
        for (int i = 0; i < inventory.PrimaryAmmo.Count; i++)
        {
            primaryAmmoHolder.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = inventory.PrimaryAmmo[i].amount.ToString();
            ItemObject tempItem = inventory.PrimaryAmmo[0].item;
            int recentAmount = inventory.PrimaryAmmo[i].amount;
            primaryAmmoHolder.GetComponent<Button>().onClick.AddListener(delegate { DropAmountPanel(tempItem, recentAmount); });
        }
        for (int i = 0; i < inventory.SecondaryAmmo.Count; i++)
        {
            secondaryAmmoHolder.transform.GetChild(0).GetComponent<TextMeshProUGUI>().text = inventory.SecondaryAmmo[i].amount.ToString();
            ItemObject tempItem = inventory.SecondaryAmmo[0].item;
            int recentAmount = inventory.SecondaryAmmo[i].amount;
            secondaryAmmoHolder.GetComponent<Button>().onClick.AddListener(delegate { DropAmountPanel(tempItem, recentAmount); });
        }


    }

    private void DropAmountPanel(ItemObject item, int recentAmount)
    {
        switch (item.type)
        {
            case ITEM_TYPE.AMMO_PRIMARY:
                dropCanvas.SetActive(true);
                dropCanvas.GetComponent<DropItemUI>().item = item;
                dropCanvas.GetComponent<DropItemUI>().maxAmount.text = recentAmount.ToString();
                break;
            case ITEM_TYPE.AMMO_SECONDARY:
                dropCanvas.SetActive(true);
                dropCanvas.GetComponent<DropItemUI>().item = item;
                dropCanvas.GetComponent<DropItemUI>().maxAmount.text = recentAmount.ToString();
                break;
        }
    }

    public void DropItem(ItemObject item, int amount)
    {
        switch (item.type)
        {
            case ITEM_TYPE.AMMO_PRIMARY:
                if (amount > 0)
                {
                    inventory.PrimaryAmmo[0].amount -= amount;
                    
                    Actions.OnPrimaryAmmoChange?.Invoke(inventory.PrimaryAmmo[0].amount);
                   
                    DropItemRPC(item.prefab, DropPosition.position, amount);
                    UpdateInvUI();
                }
                break;
            case ITEM_TYPE.AMMO_SECONDARY:
                if (amount > 0)
                {
                    inventory.SecondaryAmmo[0].amount -= amount;
                    
                    Actions.OnSecondaryAmmoChange?.Invoke(inventory.SecondaryAmmo[0].amount);
                  
                    DropItemRPC(item.prefab, DropPosition.position, amount);
                    UpdateInvUI();
                }

                break;
        }

    }


    public void DropItem(ItemObject item)
    {
        //Debug.Log("drop"+item.type);
        GameObject dropItem = item.prefab;
        switch (item.type)
        {
            case ITEM_TYPE.WEAPON_MELEE:
                for (int i = 0; i < inventory.MeleeWeapon.Count; i++)
                {
                    inventory.MeleeWeapon.Remove(inventory.MeleeWeapon[i]);
                    
                    DropItemRPC(item.prefab, DropPosition.position);
                    UpdateInvUI();
                    if (IsServer)
                        Actions.OnWeaponDrop?.Invoke(WEAPON_TYPE.MELEE);
                    return;
                }
                break;
            case ITEM_TYPE.WEAPON_GUN_PRIMARY:
                for (int i = 0; i < inventory.PrimaryGun.Count; i++)
                {
                    inventory.PrimaryGun.Remove(inventory.PrimaryGun[i]);
                    
                    DropItemRPC(item.prefab, DropPosition.position);
                    
                    UpdateInvUI();
                    if (IsServer)
                        Actions.OnWeaponDrop?.Invoke(WEAPON_TYPE.MAIN_GUN);
                    return;
                }
                break;
            case ITEM_TYPE.WEAPON_GUN_SECONDARY:
                for (int i = 0; i < inventory.SecondaryGun.Count; i++)
                {
                    inventory.SecondaryGun.Remove(inventory.SecondaryGun[i]);
                    
                    DropItemRPC(item.prefab, DropPosition.position);
                    
                    UpdateInvUI();
                    if (IsServer)
                        Actions.OnWeaponDrop?.Invoke(WEAPON_TYPE.SIZE_ARM);
                    return;
                }
                break;
            case ITEM_TYPE.AMMO_PRIMARY:

                break;
            case ITEM_TYPE.AMMO_SECONDARY:

                break;
            case ITEM_TYPE.HEALING:
                for (int i = 0; i < inventory.Container.Count; i++)
                {
                    if (inventory.Container[i].item != item)
                        continue;

                    if (inventory.Container[i].amount > 1)
                    {
                        inventory.Container[i].amount--;

                        DropItemRPC(item.prefab, DropPosition.position);
                        UpdateInvUI();
                        return;
                    }
                    if (inventory.Container[i].amount <= 1)
                    {
                        inventory.Container.Remove(inventory.Container[i]);

                        DropItemRPC(item.prefab, DropPosition.position);
                        UpdateInvUI();
                        return;
                    }
                }
                break;
            case ITEM_TYPE.CURRENCY:
                for (int i = 0; i < inventory.Container.Count; i++)
                {
                    if (inventory.Container[i].item != item)
                        continue;

                    if (inventory.Container[i].amount > 1)
                    {
                        inventory.Container[i].amount--;

                        DropItemRPC(item.prefab, DropPosition.position);
                        UpdateInvUI();
                        return;
                    }
                    if (inventory.Container[i].amount <= 1)
                    {
                        inventory.Container.Remove(inventory.Container[i]);

                        DropItemRPC(item.prefab, DropPosition.position);
                        UpdateInvUI();
                        return;
                    }
                }
                break;
            case ITEM_TYPE.GRENADE:
                for (int i = 0; i < inventory.Grenade.Count; i++)
                {
                    if (inventory.Grenade[i].item != item)
                        continue;

                    if (inventory.Grenade[i].amount > 1)
                    {
                        inventory.Grenade[i].amount--;
                        Actions.OnGrenadeNumberChange?.Invoke(inventory.Grenade[i].amount);
                       
                        DropItemRPC(item.prefab, DropPosition.position);
                        UpdateInvUI();
                        return;
                    }
                    if (inventory.Grenade[i].amount <= 1)
                    {
                        inventory.Grenade.Remove(inventory.Grenade[i]);

                        Actions.OnGrenadeNumberChange?.Invoke(0);
                        DropItemRPC(item.prefab, DropPosition.position);
                        UpdateInvUI();
                        return;
                    }
                }
                break;
        }
    }

    public void SetPlayerName(string name)
    {
        playerName.text = name;
    }

    #region MonoBehavior
    private void Awake()
    {
        inventory = new InventoryObject();
        networkCharacter = GetComponent<NetworkCharacter>();
        weaponsController = GetComponent<WeaponsController>();
        inventory.AddPrimaryAmmo(PrimaryAmmoObject, 30);
        Actions.OnPrimaryAmmoChange?.Invoke(30);
        inventory.AddSecondaryAmmo(SecondaryAmmoObject, 15);
        Actions.OnSecondaryAmmoChange?.Invoke(15);
    }
   
    #endregion

    #region NetworkBehavior
    public override void OnStartClient()
    {
        base.OnStartClient();
        ClientDoneSpawning();
        if (!base.IsOwner)
        {
            enabled = false;
            return;
        }

        cam = Camera.main;

        worldObjectHolder = GameObject.FindGameObjectWithTag("WorldObjects").transform;

        if (base.IsClient)
        {
            if (invPanel.activeSelf)
                ToggleInventory();
            strenght.text = GetComponent<PlayerStatsManager>().Strength + "";
            agility.text = GetComponent<PlayerStatsManager>().Agility + "";
            intel.text = GetComponent<PlayerStatsManager>().Intelligent + "";
        }

    }

    public void ReloadPrimaryAmmo(int amount)
    {
        inventory.PrimaryAmmo[0].amount -= amount;
        Actions.OnPrimaryAmmoChange?.Invoke(inventory.PrimaryAmmo[0].amount);
    }

    public void ReloadSecondaryAmmo(int amount)
    {
        inventory.SecondaryAmmo[0].amount -= amount;
        Actions.OnPrimaryAmmoChange?.Invoke(inventory.SecondaryAmmo[0].amount);
    }
    #endregion

    #region RPCs



    [ServerRpc(RunLocally = true)]
    private void ClientDoneSpawning()
    {
        Actions.OnReloadPrimaryAmmo += ReloadPrimaryAmmo;
        Actions.OnReloadSecondaryAmmo += ReloadSecondaryAmmo;
    }

    [ServerRpc]
    void DespawnObject(GameObject objToDespawn)
    {
        ServerManager.Despawn(objToDespawn);
    }

    [ServerRpc(RequireOwnership = false)]
    void DropItemRPC(GameObject prefab, Vector3 position)
    {
        //DropItemObserver(prefab, position);
        GameObject drop = Instantiate(prefab, position, Quaternion.identity, worldObjectHolder);
        Spawn(drop);
    }

    [ServerRpc(RequireOwnership = false)]
    void DropItemRPC(GameObject prefab, Vector3 position, int amount)
    {

        //DropItemObserver(prefab, position);
        
        GameObject drop = Instantiate(prefab, position, Quaternion.identity, worldObjectHolder);
        
       
        Spawn(drop);
        drop.GetComponent<GroundItem>().SetAmount(amount);

    }

    [ServerRpc]
    public void RequestPickup(Transform player, Transform item, NetworkConnection connection)
    {
        float distance = 3f;
        if (Vector3.Distance(player.position, item.position) <= distance)
        {
            //Debug.Log("Allowed");
            AllowPickup(connection, item);
        }
    }

    [TargetRpc(RunLocally = true)]
    public void AllowPickup(NetworkConnection conn, Transform item)
    {
        //Debug.Log("Allow Pick up amount: " + item.GetComponent<GroundItem>().amount);
        ClassifyInInventory(item.GetComponent<GroundItem>().item, item.GetComponent<GroundItem>().amount);
        DespawnObject(item.gameObject);
    }

    #endregion

}