using Cinemachine;
using FishNet;
using FishNet.Object;
using FishNet.Object.Prediction;
using FishNet.Object.Synchronizing;
using FishNet.Transporting;
using TMPro;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.Animations.Rigging;
using FishNet.Component.Animating;
using System;
using FishNet.Serializing.Helping;
using System.Collections.Generic;
using System.Collections;
using FishNet.Connection;

public class NetworkPlayer : NetworkBehaviour
{
    public float NormalSensitivity = 1f;
    public float AimSensitivity = 0.5f;
    public GameObject CameraTargetFollow;
    public GameObject CameraTargetPoint;
    public float TopClamp = 70.0f;
    public float BottomClamp = -30.0f;
    public float CameraAngleOverride = 0.0f;
    public bool LockCameraPosition = false;

    [SyncVar(Channel = Channel.Reliable, OnChange = nameof(OnKilledCountUpdate))]
    public int KillCount = 0;


    public AudioClip LandingAudioClip;
    public AudioClip[] FootstepAudioClips;
    [Range(0, 1)] public float FootstepAudioVolume = 0.5f;

    public TextMeshProUGUI playerNameUI;

    

    //Network Variables
    [SyncVar(Channel = Channel.Reliable, OnChange = nameof(OnPlayerSetName))]
    public string playerName;

    [SyncVar(Channel = Channel.Unreliable)]
    private float rigWeight;


    // cinemachine
    private float _cinemachineTargetYaw;
    private float _cinemachineTargetPitch;
    private float _sensitivity = 1f;
    private const float _threshold = 0.01f;


    // animation IDs
    private int _animIDSpeed;
    private int _animIDGrounded;
    private int _animIDJump;
    private int _animIDFreeFall;
    private int _animIDMotionSpeed;
    private int _animIDDirectionX;
    private int _animIDDirectionZ;
    private int _animIDisDead;
    private int _animIDskyFalling;
    private int _animIDskyDiving;
    private int _animIDquickDiving;

    //Components
    private PlayerInput _playerInput;
    private Animator _animator;
    private CharacterController _controller;
    private InputManager _localInput;
    private GameObject _mainCamera;
    private CinemachineVirtualCamera _aimCamera;
    private Rig _rig;
    private Health _health;
    private PlayerStatsManager statsManager;
    private CCMotor motor;
    private PlayerStates playerState;

    //Events
    public event Action StartTeleport;
    public event Action EndTeleport;


    //MoveData for client simulation
    private InputData _clientMoveData;

    #region Initialization

    private void OnPlayerSetName(string prev, string next, bool asServer)
    {
        playerNameUI.text = next;
    }

    private void OnKilledCountUpdate(int prev, int next, bool asServer)
    {
        if(base.IsOwner)
            FindObjectOfType<MatchManager>().UpdateKilledCount(next);
    }

    private void AssignAnimationIDs()
    {
        _animIDSpeed = Animator.StringToHash("Speed");
        _animIDGrounded = Animator.StringToHash("Grounded");
        _animIDJump = Animator.StringToHash("Jump");
        _animIDFreeFall = Animator.StringToHash("FreeFall");
        _animIDMotionSpeed = Animator.StringToHash("MotionSpeed");
        _animIDDirectionX = Animator.StringToHash("DirectionX");
        _animIDDirectionZ = Animator.StringToHash("DirectionZ");
        _animIDisDead = Animator.StringToHash("isDead");
        _animIDskyDiving = Animator.StringToHash("isSkyDiving");
        _animIDquickDiving = Animator.StringToHash("QuickDive");
        _animIDskyFalling = Animator.StringToHash("SkyFalling");
    }

    private void Awake()
    {
        InstanceFinder.TimeManager.OnTick += TimeManager_OnTick;
        InstanceFinder.TimeManager.OnUpdate += TimeManager_OnUpdate;

        CacheComponents();
    }

    private void OnDestroy()
    {
        if (InstanceFinder.TimeManager != null)
        {
            InstanceFinder.TimeManager.OnTick -= TimeManager_OnTick;
            InstanceFinder.TimeManager.OnUpdate -= TimeManager_OnUpdate;
        }
        //_health.Die -= OnDie;
    }

    private void CacheComponents()
    {
        _aimCamera = FindObjectOfType<VirtualCameraManager>().aimCamera.GetComponent<CinemachineVirtualCamera>();
        _rig = GetComponentInChildren<Rig>();
        _mainCamera = GameObject.FindGameObjectWithTag("MainCamera");
        _localInput = FindObjectOfType<InputManager>();
        _health = GetComponent<Health>();
        //_health.Die += OnDie;
        statsManager = GetComponent<PlayerStatsManager>();
        _animator = GetComponent<Animator>();
        motor = GetComponent<CCMotor>();
        _controller = GetComponent<CharacterController>();
        playerState = GetComponent<PlayerStates>();
    }


    public override void OnStartClient()
    {
        base.OnStartClient();

        bool authorityCheck = (base.IsServer || base.IsOwner);
        _controller.enabled = authorityCheck;

        if (base.IsOwner)
        {
            GameObject.FindGameObjectWithTag("PlayerFollowCamera").GetComponent<CinemachineVirtualCamera>().Follow = CameraTargetFollow.transform;
            _aimCamera.Follow = CameraTargetFollow.transform;
            
            _playerInput = FindObjectOfType<InputManager>().GetComponent<PlayerInput>();
            _playerInput.enabled = true;

            _localInput.SetCursorState(true);
            playerNameUI.gameObject.SetActive(false);
            SetPlayerName(PlayerLobbyData.Instance.playerName);
        }
    }

    public override void OnStartNetwork()
    {
        base.OnStartNetwork();

        _cinemachineTargetYaw = CameraTargetFollow.transform.rotation.eulerAngles.y;

        AssignAnimationIDs();

        if (base.IsServer)
        {
            motor.MoveSpeed = statsManager.GetSpeed();
            motor.SprintSpeed = motor.MoveSpeed + 3f;
        }
    }

    [ServerRpc]
    private void SetPlayerName(string name)
    {
        playerName = name;
    }

    #endregion

    #region Update and TickUpdate

    private void TimeManager_OnTick()
    {
        if (base.IsOwner)
        {
            Reconciliation(default, false);
            CheckInput(out InputData inputData);
            Simulate(inputData, false);
        }
        if (base.IsServer)
        {
            Simulate(default, true);
            ReconcileData rd = new ReconcileData(transform.position, transform.rotation, motor.verticalVelocity, motor.FallTimeoutDelta, motor.JumpTimeoutDelta, motor.Grounded, playerState.state,
                motor.MoveSpeed, motor.SprintSpeed);
            Reconciliation(rd, true);
        }
    }

    private void TimeManager_OnUpdate()
    {
        if (base.IsOwner)
        {
            ProcessInput(_clientMoveData, Time.deltaTime);
        }
    }


    private void LateUpdate()
    {
        if (base.IsOwner)
            CameraRotation();
    }

    #endregion

    [Reconcile]
    private void Reconciliation(ReconcileData rd, bool asServer)
    {
        if (Comparers.IsDefault(rd))
            return;

        transform.position = rd.Position;
        transform.rotation = rd.Rotation;
        motor.verticalVelocity = rd.VerticalVelocity;
        motor.FallTimeoutDelta = rd.FallTimeout;
        motor.JumpTimeoutDelta = rd.JumpTimeout;
        motor.Grounded = rd.Grounded;
        playerState.state = rd.States;
        motor.MoveSpeed = rd.MoveSpeed;
        motor.SprintSpeed = rd.SprintSpeed;

    }    

    [Replicate]
    private void Simulate(InputData inputData, bool asServer, bool replaying = false)
    {
        if (asServer || replaying)
        {
            ProcessInput(inputData, (float)base.TimeManager.TickDelta);
        }
        else if (!asServer)
            _clientMoveData = inputData;
    }

    private void ProcessInput(InputData inputData, float deltaTime)
    {
        if (playerState.CanAim())
            Aim(inputData, deltaTime);

        if (playerState.CanMove())
            motor.ProcessInput(inputData, deltaTime);

        

    }

    [ServerRpc(RunLocally = true)]
    private void Teleport()
    {
        _controller.enabled = false;
        gameObject.transform.position = new Vector3(0, 0, 0);
        _controller.enabled = true;
        _health.isInvincible = true;
        playerState.state = State.Teleporting;
        if(IsOwner)
            StartTeleport?.Invoke();
        StartCoroutine(TeleportLoading(3));
    }

    IEnumerator TeleportLoading(float time)
    {
        yield return new WaitForSeconds(time);
        _health.isInvincible = false;
        playerState.state = State.Normal;
        if(IsOwner)
            EndTeleport?.Invoke();
    }

    private void CheckInput(out InputData inputData)
    {
        inputData = new InputData()
        {
            Move = _localInput.move,
            Jump = _localInput.jump,
            CameraEulerY = _mainCamera.transform.eulerAngles.y,
            Sprint = _localInput.sprint,
            Aim = _localInput.aim,
            Skill_1 = _localInput.skill_1,
            Skill_2 = _localInput.skill_2,
            Skill_3 = _localInput.skill_3,
        };

        _localInput.jump = false;

        if (_localInput.skill_1)
            Teleport();
        _localInput.skill_1 = false;
    }

    private void CameraRotation()
    {
        if (Cursor.lockState == CursorLockMode.None) LockCameraPosition = true;
        else LockCameraPosition = false;
        if (_localInput.look.sqrMagnitude >= _threshold && !LockCameraPosition)
        {
            _cinemachineTargetYaw += _localInput.look.x * 0.05f;
            _cinemachineTargetPitch += _localInput.look.y * 0.05f;
        }

        _cinemachineTargetYaw = ClampAngle(_cinemachineTargetYaw, float.MinValue, float.MaxValue);
        _cinemachineTargetPitch = ClampAngle(_cinemachineTargetPitch, BottomClamp, TopClamp);

        CameraTargetPoint.transform.rotation = Quaternion.Euler(_cinemachineTargetPitch + CameraAngleOverride,
            _cinemachineTargetYaw, 0.0f);

        if(playerState.state == State.Aiming)
        {
            CameraTargetFollow.transform.rotation = CameraTargetPoint.transform.rotation;
        }
        else
        {
            CameraTargetFollow.transform.rotation = CameraTargetPoint.transform.rotation;
        }
    }

    public void AddCameraYaw(float amount)
    {
        _cinemachineTargetYaw += amount;
    }

    public void AddCameraPitch(float amount)
    {
        _cinemachineTargetPitch += amount;
    }

    private void Aim(InputData inputData, float delta)
    {
        if (inputData.Aim)
        {
            _animator.SetLayerWeight(1, 1);
            if(base.IsOwner)
                _aimCamera.gameObject.SetActive(true);
            _sensitivity = AimSensitivity;
            ChangeRigWeight(1f);
            playerState.state = State.Aiming;
        }
        else
        {
            _animator.SetLayerWeight(1, 0);
            if (base.IsOwner)
                _aimCamera.gameObject.SetActive(false);
            _sensitivity = NormalSensitivity;
            ChangeRigWeight(0f);
            playerState.state = State.Normal;
        }
    }

    [ObserversRpc(RunLocally = true)]
    private void ChangeRigWeight(float targetWeight)
    {
        _rig.weight = Mathf.Lerp(_rig.weight, targetWeight, Time.deltaTime * 20F);
    }

    private static float ClampAngle(float lfAngle, float lfMin, float lfMax)
    {
        if (lfAngle < -360f) lfAngle += 360f;
        if (lfAngle > 360f) lfAngle -= 360f;
        return Mathf.Clamp(lfAngle, lfMin, lfMax);
    }

    
    private void OnFootstep(AnimationEvent animationEvent)
    {
        if (!base.IsOwner) return;

        if (animationEvent.animatorClipInfo.weight > 0.5f)
        {
            if (FootstepAudioClips.Length > 0)
            {
                var index = UnityEngine.Random.Range(0, FootstepAudioClips.Length);
                AudioSource.PlayClipAtPoint(FootstepAudioClips[index], transform.TransformPoint(_controller.center), FootstepAudioVolume);
            }
        }
    }

    private void OnLand(AnimationEvent animationEvent)
    {
        if (!base.IsOwner) return;

        if (animationEvent.animatorClipInfo.weight > 0.5f)
        {
            AudioSource.PlayClipAtPoint(LandingAudioClip, transform.TransformPoint(_controller.center), FootstepAudioVolume);
        }
    }

    private void FootL()
    {

    }

    private void FootR()
    {

    }



    public void OnDie()
    {
        playerState.state = State.Dying;
        _animator.applyRootMotion = true;
        _animator.SetBool(_animIDisDead, true);
    }
}

[Serializable]
public struct CharacterStates
{
    public bool isControllable;
    public bool isMovable;
    public bool isAiming;
    public bool isShooting;
    public bool isPerformingSkill;
    public bool isAlive;
    public bool isSkydiving;

    public CharacterStates(bool isNew)
    {
        isControllable = true;
        isMovable = true;
        isAiming = false;
        isShooting = false;
        isPerformingSkill = false;
        isAlive = true;
        isSkydiving = false;
    }
}

public struct InputData
{
    public Vector2 Move;
    public bool Jump;
    public float CameraEulerY;
    public bool Sprint;
    public bool Aim;
    public bool Skill_1;
    public bool Skill_2;
    public bool Skill_3;
}

//ReconcileData for Reconciliation
public struct ReconcileData
{
    public Vector3 Position;
    public Quaternion Rotation;
    public float VerticalVelocity;
    public float FallTimeout;
    public float JumpTimeout;
    public bool Grounded;
    public State States;
    public float MoveSpeed;
    public float SprintSpeed;

    public ReconcileData(Vector3 position, Quaternion rotation, float verticalVelocity, float fallTimeout, float jumpTimeout, bool grounded, State states, float moveSpeed, float sprintSpeed)
    {
        Position = position;
        Rotation = rotation;
        VerticalVelocity = verticalVelocity;
        FallTimeout = fallTimeout;
        JumpTimeout = jumpTimeout;
        Grounded = grounded;
        States = states;
        MoveSpeed = moveSpeed;
        SprintSpeed = sprintSpeed;

    }
}