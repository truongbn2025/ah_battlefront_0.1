using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CCMotor : MonoBehaviour
{
    //Component
    private Animator animator;
    private PlayerStates playerState;
    private CharacterController controller;


    //Public members
    public float MoveSpeed = 2.0f;
    public float SprintSpeed = 6f;
    public float RotationSmoothTime = 12f;
    public float SpeedChangeRate = 10.0f;
    public float JumpHeight = 1.2f;
    public float Gravity = -15.0f;
    public float verticalVelocity;
    public float JumpTimeout = 0.50f;
    public float FallTimeout = 0.15f;
    public bool Grounded = true;
    public float GroundedOffset = -0.14f;
    public float GroundedRadius = 0.28f;
    public LayerMask GroundLayers;
    

    //Private members
    private float speed;
    private float animationSpeedBlend;
    private float targetRotation = 0.0f;
    private float rotationVelocity;
    private float terminalVelocity = 53.0f;
    private bool leaveGroundCheck;
    private float _groundRayDistance = 1f;
    private RaycastHit _slopeHit;
    private float _slideSpeed = 4f;

    [HideInInspector]
    public float JumpTimeoutDelta;
    [HideInInspector]
    public float FallTimeoutDelta;

    private void Awake()
    {
        CacheComponents();
    }

    private void CacheComponents()
    {
        animator = GetComponent<Animator>();
        playerState = GetComponent<PlayerStates>();
        controller = GetComponent<CharacterController>();
    }

    public void ProcessInput(InputData inputData, float deltaTime)
    {
        GroundedCheck();
        JumpAndGravity(inputData, deltaTime);
        switch(playerState.state)
        {
            case State.Normal:
                Move(inputData, deltaTime);
                break;
            case State.SkyDiving:
                SkyDive(inputData, deltaTime);
                break;
            case State.Aiming:
                AimMove(inputData, deltaTime);
                break;
        }
        
    }

    private void GroundedCheck()
    {
        Vector3 spherePosition = new Vector3(transform.position.x, transform.position.y - GroundedOffset, transform.position.z);
        Grounded = Physics.CheckSphere(spherePosition, GroundedRadius, GroundLayers, QueryTriggerInteraction.Ignore);

        animator.SetBool("Grounded", Grounded);
    }

    private void Move(InputData inputData, float delta)
    {
        float targetSpeed = inputData.Sprint ? SprintSpeed : MoveSpeed;
        Vector3 inputDirection = new Vector3(inputData.Move.x, 0.0f, inputData.Move.y).normalized;

        if (inputData.Move == Vector2.zero) 
            targetSpeed = 0.0f;
        else
        {
            targetRotation = Mathf.Atan2(inputDirection.x, inputDirection.z) * Mathf.Rad2Deg + inputData.CameraEulerY;
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0.0f, targetRotation, 0.0f), RotationSmoothTime * delta);
        }

        Vector3 targetDirection = Quaternion.Euler(0.0f, targetRotation, 0.0f) * Vector3.forward;

        if (checkForSteepSlope() && verticalVelocity <= 0f)
        {
            controller.Move(steepSlopeMovement(delta));
        }
        else
        {
            controller.Move(targetDirection.normalized * (targetSpeed * delta) + new Vector3(0.0f, verticalVelocity, 0.0f) * delta);
        }

        animationSpeedBlend = Mathf.Lerp(animationSpeedBlend, targetSpeed, delta * SpeedChangeRate);
        if (animationSpeedBlend < 0.01f) animationSpeedBlend = 0f;
        animator.SetFloat("Speed", animationSpeedBlend);
        animator.SetFloat("DirectionX", Mathf.Lerp(animator.GetFloat("DirectionX"), inputData.Move.x, Time.deltaTime * 10f));
        animator.SetFloat("DirectionZ", Mathf.Lerp(animator.GetFloat("DirectionZ"), inputData.Move.y, Time.deltaTime * 10f));
    }

    private void AimMove(InputData inputData, float delta)
    {
        float targetSpeed = MoveSpeed * 0.7f;
        Vector3 inputDirection = new Vector3(inputData.Move.x, 0.0f, inputData.Move.y).normalized;
        transform.rotation = Quaternion.Lerp(transform.rotation, Quaternion.Euler(0.0f, inputData.CameraEulerY, 0.0f), delta * 20f);


        if (inputData.Move == Vector2.zero) 
            targetSpeed = 0.0f;
        else
        {
            targetRotation = Mathf.Atan2(inputDirection.x, inputDirection.z) * Mathf.Rad2Deg + inputData.CameraEulerY;
        }

        Vector3 targetDirection = Quaternion.Euler(0.0f, targetRotation, 0.0f) * Vector3.forward;

        if (checkForSteepSlope() && verticalVelocity <= 0f)
        {
            controller.Move(steepSlopeMovement(delta));
        }
        else
        {
            controller.Move(targetDirection.normalized * (targetSpeed * delta) + new Vector3(0.0f, verticalVelocity, 0.0f) * delta);
        }

        animationSpeedBlend = Mathf.Lerp(animationSpeedBlend, targetSpeed, delta * SpeedChangeRate);
        if (animationSpeedBlend < 0.01f) animationSpeedBlend = 0f;
        animator.SetFloat("Speed", animationSpeedBlend);
        animator.SetFloat("DirectionX", Mathf.Lerp(animator.GetFloat("DirectionX"), inputData.Move.x, Time.deltaTime * 10f));
        animator.SetFloat("DirectionZ", Mathf.Lerp(animator.GetFloat("DirectionZ"), inputData.Move.y, Time.deltaTime * 10f));
    }

    private void SkyDive(InputData inputData, float delta)
    {
        //float targetSpeed = inputData.Sprint ? SprintSpeed : MoveSpeed;

        //animator.SetBool("isSkyDiving", true);
        //inputData.Move.x = 0;
        //if (inputData.Move.y < 0) inputData.Move.y = 0;
        //if (inputData.Sprint)
        //{
        //    targetSpeed += 70;
        //    animator.SetBool("QuickDive", true);
        //    RotationSmoothTime = 0.8f;
        //}
        //else
        //{
        //    targetSpeed += 50;
        //    animator.SetBool("QuickDive", false);
        //    RotationSmoothTime = 2f;
        //}
        //SpeedChangeRate = 2f;

        //float currentHorizontalSpeed = new Vector3(controller.velocity.x, 0.0f, controller.velocity.z).magnitude;
        //float speedOffset = 0.1f;

        //if (currentHorizontalSpeed < targetSpeed - speedOffset ||
        //    currentHorizontalSpeed > targetSpeed + speedOffset)
        //{
        //    speed = Mathf.Lerp(speed, targetSpeed,
        //        delta * SpeedChangeRate);

        //    speed = Mathf.Round(speed * 1000f) / 1000f;
        //}
        //else
        //{
        //    speed = targetSpeed;
        //}


        //if (inputData.Move == Vector2.zero) speed = 0.0f;

        ////speed = targetSpeed;


        //Vector3 inputDirection = new Vector3(inputData.Move.x, 0.0f, inputData.Move.y).normalized;

        //if (inputData.Move != Vector2.zero)
        //{
        //    targetRotation = Mathf.Atan2(inputDirection.x, inputDirection.z) * Mathf.Rad2Deg + inputData.CameraEulerY;

        //    transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0.0f, targetRotation, 0.0f), RotationSmoothTime * delta);

        //}

        //Vector3 targetDirection;
        //    targetDirection = transform.rotation * Vector3.forward;


        //    controller.Move(targetDirection.normalized * (speed * delta) + new Vector3(0.0f, verticalVelocity, 0.0f) * delta);


        animator.SetBool("isSkyDiving", true);
        if(inputData.Sprint)
            animator.SetBool("QuickDive", true);
        else
            animator.SetBool("QuickDive", false);

        float targetSpeed = inputData.Sprint ? SprintSpeed : MoveSpeed;
        Vector3 inputDirection = new Vector3(inputData.Move.x, 0.0f, inputData.Move.y).normalized;

        if (inputData.Move == Vector2.zero)
            speed = 0.0f;
        else
        {
            inputData.Move.x = 0;
            if (inputData.Move.y < 0) inputData.Move.y = 0;

            if (inputData.Sprint)
            {
                targetSpeed += 70;
                RotationSmoothTime = 0.8f;
            }
            else
            {
                targetSpeed += 50;
                RotationSmoothTime = 2f;
            }

            SpeedChangeRate = 2f;

            speed = targetSpeed;

            targetRotation = Mathf.Atan2(inputDirection.x, inputDirection.z) * Mathf.Rad2Deg + inputData.CameraEulerY;
            transform.rotation = Quaternion.Slerp(transform.rotation, Quaternion.Euler(0.0f, targetRotation, 0.0f), RotationSmoothTime * delta);
        }

        Vector3 targetDirection = transform.rotation * Vector3.forward;
        controller.Move(targetDirection.normalized * (speed * delta) + new Vector3(0.0f, verticalVelocity, 0.0f) * delta);
    }

    private bool checkForSteepSlope()
    {
        if (!Grounded) return false;

        if (Physics.Raycast(transform.position, Vector3.down, out _slopeHit, (controller.height / 2) + _groundRayDistance))
        {
            float _slopeAngle = Vector3.Angle(_slopeHit.normal, Vector3.up);
            if (_slopeAngle > controller.slopeLimit)
            {
                return true;
            }
        }
        return false;
    }

    private Vector3 steepSlopeMovement(float delta)
    {
        Vector3 slopeDirection = Vector3.up - _slopeHit.normal * Vector3.Dot(Vector3.up, _slopeHit.normal);

        Vector3 moveDir = slopeDirection.normalized * -_slideSpeed;
        moveDir.y -= _slopeHit.point.y + 10f;
        return moveDir * delta;
    }

    private void EndSkyDivingState()
    {
        playerState.state = State.Normal;
        animator.SetBool("isSkyDiving", false);
        RotationSmoothTime = 12;
        SpeedChangeRate = 10f;
    }

    private void JumpAndGravity(InputData inputData, float delta)
    {
        if (Grounded)
        {
            leaveGroundCheck = true;
            if (playerState.state == State.SkyDiving)
            {
                EndSkyDivingState();
            }

            // reset the fall timeout timer
            FallTimeoutDelta = FallTimeout;

            leaveGroundCheck = true;

            // update animator if using character
            animator.SetBool("Jump", false);
            animator.SetBool("FreeFall", false);

            // stop our velocity dropping infinitely when grounded
            if (verticalVelocity < 0.0f)
            {
                verticalVelocity = -5f;

            }

            // Jump
            if (inputData.Jump && JumpTimeoutDelta <= 0.0f && playerState.CanJump() && !checkForSteepSlope())
            {
                verticalVelocity = -2f;
                // the square root of H * -2 * G = how much velocity needed to reach desired height
                verticalVelocity = Mathf.Sqrt(JumpHeight * -2f * Gravity);

                // update animator if using character
                animator.SetBool("Jump", true);
                leaveGroundCheck = false;
            }

            // jump timeout
            if (JumpTimeoutDelta >= 0.0f)
            {
                JumpTimeoutDelta -= delta;
            }
        }
        else
        {
            if (leaveGroundCheck)
            {
                verticalVelocity = -2f;
                leaveGroundCheck = false;
            }

            // reset the jump timeout timer
            JumpTimeoutDelta = JumpTimeout;

            // fall timeout
            if (FallTimeoutDelta >= 0.0f)
            {
                FallTimeoutDelta -= delta;
            }
            else
            {
                // update animator if using character
                animator.SetBool("FreeFall", true);
            }
        }

        // apply gravity over time if under terminal (multiply by delta time twice to linearly speed up over time)
        if (verticalVelocity < terminalVelocity && playerState.state != State.SkyDiving)
        {
            verticalVelocity += Gravity * delta;
        }
        else if (playerState.state == State.SkyDiving)
        {
            if (inputData.Sprint)
                verticalVelocity = -50;
            else
                verticalVelocity = -25;
        }
    }
}
