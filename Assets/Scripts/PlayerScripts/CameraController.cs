using Cinemachine;
using FishNet.Object;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CameraState
{
    Normal,
    Aiming,
    Vehicle,
    Dropship
}

public class CameraController : NetworkBehaviour
{
    public CameraState cameraState;

    public Transform cameraTarget;
    public float NormalSensitivity = 1f;
    public bool ToggleAim = false;
    public float AimSensitivity = 0.5f;
    public float TopClamp = 90.0f;
    public float BottomClamp = -80.0f;
    public float AimTopClamp = 60f;
    public float AimBottomClamp = -60f;
    public float CameraAngleOverride = 0.0f;
    public bool LockCameraPosition = false;
    public float AimCameraOffset = 0.3f;

    private VirtualCameraManager virtualCameraManager;
    private InputManager localInput;
    private WeaponsController weaponsController;

    public float cinemachineTargetYaw { get; private set; }
    public float cinemachineTargetPitch { get; private set; }

    private const float threshold = 0.01f;

    private float targetCameraSide;
    private Cinemachine3rdPersonFollow aimCam;


    private void Awake()
    {
        cacheComponent();
    }

    private void Update()
    {
        if (!IsOwner) return;

        if ((weaponsController.combatState == CombatState.LeftAiming || weaponsController.combatState == CombatState.RightAiming) && cameraState == CameraState.Normal)
        {
            OnAim();
        }
        else if (weaponsController.combatState == CombatState.Normal)
        {
            OnUnAim();
        }

        if(localInput.switchCameraSide)
        {
            if (aimCam.CameraSide == 0.5f - AimCameraOffset)
                targetCameraSide = 0.5f + AimCameraOffset;
            else
                targetCameraSide = 0.5f - AimCameraOffset;

            localInput.switchCameraSide = false;
        }

        

        if (Mathf.Abs(aimCam.CameraSide - targetCameraSide) > 0.01f)
            aimCam.CameraSide = Mathf.Lerp(aimCam.CameraSide, targetCameraSide, Time.deltaTime * 20f);
        else
            aimCam.CameraSide = targetCameraSide;
    }

    private void OnAim()
    {
        virtualCameraManager.normalCamera.SetActive(false);
        virtualCameraManager.aimCamera.SetActive(true);
        targetCameraSide = (weaponsController.combatState == CombatState.LeftAiming) ? 0.5f - AimCameraOffset : 0.5f + AimCameraOffset;
        cameraState = CameraState.Aiming;
    }

    private void OnUnAim()
    {
        virtualCameraManager.normalCamera.SetActive(true);
        virtualCameraManager.aimCamera.SetActive(false);
        cameraState = CameraState.Normal;
    }

    private void cacheComponent()
    {
        virtualCameraManager = FindObjectOfType<VirtualCameraManager>();
        localInput = FindObjectOfType<InputManager>();
        weaponsController = GetComponent<WeaponsController>();
        aimCam = virtualCameraManager.aimCamera.GetComponent<CinemachineVirtualCamera>().GetCinemachineComponent<Cinemachine3rdPersonFollow>();
    }

    public override void OnStartClient()
    {
        base.OnStartClient();


        if (base.IsOwner)
        {
            virtualCameraManager.normalCamera.GetComponent<CinemachineVirtualCamera>().Follow = cameraTarget;
            virtualCameraManager.aimCamera.GetComponent<CinemachineVirtualCamera>().Follow = cameraTarget;
        }
    }

    private void LateUpdate()
    {
        if (IsOwner)
            CameraRotation();
    }

    private void CameraRotation()
    {
        if (Cursor.lockState == CursorLockMode.None) LockCameraPosition = true;
        else LockCameraPosition = false;

        float sensitivity = localInput.aim? AimSensitivity : NormalSensitivity;

        if (localInput.look.sqrMagnitude >= threshold && !LockCameraPosition)
        {
            cinemachineTargetYaw += localInput.look.x * 0.05f * sensitivity;
            cinemachineTargetPitch += localInput.look.y * 0.05f * sensitivity;
        }

        cinemachineTargetYaw = ClampAngle(cinemachineTargetYaw, float.MinValue, float.MaxValue);
        if(cameraState == CameraState.Aiming)
            cinemachineTargetPitch = ClampAngle(cinemachineTargetPitch, AimBottomClamp, AimTopClamp);
        else
            cinemachineTargetPitch = ClampAngle(cinemachineTargetPitch, BottomClamp, TopClamp);

        cameraTarget.transform.rotation = Quaternion.Euler(cinemachineTargetPitch + CameraAngleOverride,
            cinemachineTargetYaw, 0.0f);
    }

    public void AddCameraYaw(float amount)
    {
        cinemachineTargetYaw += amount;
    }

    public void AddCameraPitch(float amount)
    {
        cinemachineTargetPitch += amount;
    }

    private static float ClampAngle(float lfAngle, float lfMin, float lfMax)
    {
        if (lfAngle < -360f) lfAngle += 360f;
        if (lfAngle > 360f) lfAngle -= 360f;
        return Mathf.Clamp(lfAngle, lfMin, lfMax);
    }
}
