using FishNet.Object;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PersonalUIManager : NetworkBehaviour
{

    [SerializeField] private TextMeshProUGUI cooldownSkill_1;
    [SerializeField] private Image iconSkill_1;
    [SerializeField] private TextMeshProUGUI keybindSkill_1;

    [SerializeField] private TextMeshProUGUI cooldownSkill_2;
    [SerializeField] private Image iconSkill_2;
    [SerializeField] private TextMeshProUGUI keybindSkill_2;

    [SerializeField] private TextMeshProUGUI cooldownSkill_3;
    [SerializeField] private Image iconSkill_3;
    [SerializeField] private TextMeshProUGUI keybindSkill_3;

    [SerializeField] private GameObject crosshair;
    [SerializeField] private TextMeshProUGUI healthText;

    [SerializeField] private GameObject teleportPanel;

    [SerializeField] WeaponManager weaponManager;
    [SerializeField] Health health;
    [SerializeField] NetworkPlayer networkPlayer;
 
    private void OnEnable()
    {
        weaponManager.Aiming += OnAiming;
        health.HealthUpdate += OnHealthChanged;
        networkPlayer.StartTeleport += StartTeleport;
        networkPlayer.EndTeleport += EndTeleport;
    }

    private void OnDisable()
    {
        weaponManager.Aiming -= OnAiming;
        health.HealthUpdate -= OnHealthChanged;
        networkPlayer.EndTeleport -= EndTeleport;
        networkPlayer.EndTeleport -= EndTeleport;
    }

    public void OnAiming(bool aiming)
    {
        crosshair.SetActive(aiming);
    }

    public void OnHealthChanged(float health)
    {
        healthText.text = health.ToString("0");
    }

    public override void OnStartClient()
    {
        base.OnStartClient();
        if (!base.IsOwner) gameObject.SetActive(false);
    }

    private void Update()
    {
    }

    private string GetCooldownText(float cooldown)
    {
        if (cooldown > 0f)
            return cooldown.ToString("0.0");
        else
            return "Ready";
    }

    private void StartTeleport()
    {
        teleportPanel.SetActive(true);
    }

    private void EndTeleport()
    {
        teleportPanel.SetActive(false);
    }
}
