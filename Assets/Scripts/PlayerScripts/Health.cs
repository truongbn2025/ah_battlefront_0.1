using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using FishNet.Object;
using FishNet.Object.Synchronizing;
using System;
using FishNet.Connection;

public class Health : NetworkBehaviour
{
    [SyncVar(OnChange = nameof(OnHealthChanged))]
    private float health;

    //[SyncVar]
    public bool isInvincible = true;
    public bool isAlive = true;

    public event Action<float> HealthUpdate;
    public event Action<NetworkCharacter, string> Die;
    private HurtBox[] hurtBoxes;
    private PlayerStatsManager statsManager;

    private void Awake()
    {
        hurtBoxes = GetComponentsInChildren<HurtBox>();
        statsManager = GetComponent<PlayerStatsManager>();
    }

    public override void OnStartNetwork()
    {
        base.OnStartNetwork();
        if(base.IsServer)
            health = statsManager.GetHealth();
        foreach(HurtBox hurtBox in hurtBoxes)
        {
            hurtBox.hit += ReduceHealth;
        }
    }

    public void OnDestroy()
    {
        foreach (HurtBox hurtBox in hurtBoxes)
        {
            hurtBox.hit -= ReduceHealth;
        }
    }

    public void ReduceHealth(float amount, NetworkCharacter source, string weaponName)
    {
        if (isInvincible || !isAlive) return;

        health -= Mathf.Round((amount - statsManager.GetResistance()));

        if (health <= 0)
        {
            health = 0;
            isAlive = false;
            Die?.Invoke(source, weaponName);
            source.KillCount++;
            GetComponent<NetworkCharacter>().characterState = CharacterState.Dead;
        }
    }   

    private void OnHealthChanged(float prev, float next, bool asServer)
    {
        if (asServer) return;
        HealthUpdate?.Invoke(next);
        if (health <= 0)
        {
            //GetComponent<NetworkCharacter>().OnDie(); 
            isAlive = false;
        }
    }

    public void SetIgnoreRaycast(bool isIgnore)
    {
        foreach (HurtBox hurtBox in hurtBoxes)
        {
            if (isIgnore) hurtBox.gameObject.layer = 2;
            else hurtBox.gameObject.layer = 3;
        }
    }
}
