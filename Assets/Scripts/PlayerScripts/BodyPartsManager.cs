using FishNet.Connection;
using FishNet.Object;
using FishNet.Object.Synchronizing;
using FishNet.Transporting;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public struct PartID
{
    public int Head;
    public int Body;
    public int Thight;
    public int LArm;
    public int RArm;
    public int LLeg;
    public int RLeg;

}

public struct BodyObject
{
    public Heads Head;
    public Bodies Body;
    public Hips Thight;
    public LeftArms LArm;
    public RightArms RArm;
    public LeftLegs LLeg;
    public RightArms RLeg;
    
}

public class BodyPartsManager : NetworkBehaviour
{
    public List<GameObject> Heads;
    public List<GameObject> Bodys;
    public List<GameObject> Thights;
    public List<GameObject> LArms;
    public List<GameObject> RArms;
    public List<GameObject> LLegs;
    public List<GameObject> RLegs;

    public event Action LoadingPartReady;

    [SyncVar(Channel = Channel.Reliable, OnChange = nameof(OnPartIDChanged))]
    public PartID partID = new PartID();
    [SyncVar(Channel = Channel.Reliable)]
    public BodyObject partObject = new BodyObject();

    public override void OnStartServer()
    {
        base.OnStartServer();

        
    }

    public override void OnStartClient()
    {
        base.OnStartClient();

        if (base.IsOwner)
        {
            GenerateRandomPartID();
            //GenerateRandomSetID();
        }
    }


    [ServerRpc]
    private void GenerateRandomPartID()
    {
        print("Calledd");
        PartID newPart = new PartID();
        newPart.Head = UnityEngine.Random.Range(0, 4);
        newPart.Body = UnityEngine.Random.Range(0, 4);
        newPart.Thight = UnityEngine.Random.Range(0, 4);
        newPart.LArm = UnityEngine.Random.Range(0, 4);
        newPart.RArm = 1;
        newPart.LLeg = UnityEngine.Random.Range(0, 4);
        newPart.RLeg = UnityEngine.Random.Range(0, 4);

        partID = newPart;
    }

    //this where you get the body part
    [ServerRpc]
    private void GenerateRandomSetID()
    {
        PartID newPart = new PartID();
        //int idPart = UnityEngine.Random.Range(0, 4);
        int idPart = 0;
        newPart.Head = idPart;
        newPart.Body = idPart;
        newPart.Thight = idPart;
        newPart.LArm = idPart;
        newPart.RArm = idPart;
        newPart.LLeg = idPart;
        newPart.RLeg = idPart;

        partID = newPart;
    }


    private void DisableAllPart()
    {
        foreach (var skin in Heads)
            skin.SetActive(false);
        foreach (var skin in Bodys)
            skin.SetActive(false);
        foreach (var skin in Thights)
            skin.SetActive(false);
        foreach (var skin in LArms)
            skin.SetActive(false);
        foreach (var skin in RArms)
            skin.SetActive(false);
        foreach (var skin in LLegs)
            skin.SetActive(false);
        foreach (var skin in RLegs)
            skin.SetActive(false);
    }
    private void OnPartIDChanged(PartID prev, PartID next, bool asServer)
    {
        LoadBodyParts(next);
    }
    private void LoadBodyParts(PartID partID)
    {
        DisableAllPart();
       
        Heads[partID.Head].SetActive(true);
        Bodys[partID.Body].SetActive(true);
        Thights[partID.Thight].SetActive(true);
        LArms[partID.LArm].SetActive(true);
        RArms[partID.RArm].SetActive(true);
        LLegs[partID.LLeg].SetActive(true);
        RLegs[partID.RLeg].SetActive(true);


        //GetBodyPartsObject();
        //Debug.Log(partObject.Head.partName);
        //Debug.Log(partObject.Head.partName);
        LoadingPartReady?.Invoke();
    }

    private void GetBodyPartsObject()
    {
        BodyObject thisPartObject;

        thisPartObject.Head = BodyPartFactory.Instance.Heads[0];
        thisPartObject.Body = BodyPartFactory.Instance.Bodies[0];
        thisPartObject.Thight = BodyPartFactory.Instance.Hips[0];
        thisPartObject.LArm = BodyPartFactory.Instance.LeftArms[0];
        thisPartObject.RArm = BodyPartFactory.Instance.RightArms[0];
        thisPartObject.LLeg = BodyPartFactory.Instance.LeftLegs[0];
        thisPartObject.RLeg = BodyPartFactory.Instance.RightLegs[0];

        partObject = thisPartObject;
    }

    
}
