using FishNet;
using FishNet.Managing.Timing;
using FishNet.Object;
using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using Random = UnityEngine.Random;

public class WeaponManager : NetworkBehaviour
{
    [SerializeField] private LayerMask shootingLayer;
    [SerializeField] GameObject gunBack;
    [SerializeField] GameObject gunHand;
    [SerializeField] private float bulletSpeed = 1000.0f;
    [SerializeField] private float bulletDrop = 0f;

    private InputManager _input;
    private NetworkPlayer networkPlayer;
    private Health health;
    private PlayerStates playerState;
    private Camera mainCamera;
    private Transform gunExitPoint;
    private float baseFireDelay = 0.2f;
    private float fireDelay = 0f;
    private AudioSource shootEffect;
    private Weapon currentWeapon;
    private int consecutiveShot = 0;
    private bool shootingLastFrame;
    private int currentWeaponClip;
    private bool reloading;

    public Transform CameraTargetFollow;
    public Transform CameraTargetPoint;
    public GameObject hitIndicator;
    public GameObject muzzleEffect;
    public TextMeshProUGUI Clip;
    [SerializeField]
    GameObject bulletTrail;
    public GameObject ballPrefab;
    public event Action<bool> Aiming;

    private void Awake()
    {
        shootEffect = GetComponent<AudioSource>();
        InstanceFinder.TimeManager.OnTick += TimeManager_OnTick;
        _input = FindObjectOfType<InputManager>();
        mainCamera = Camera.main;
        gunExitPoint = gunHand.GetComponent<Weapon>().ExitPoint;
        currentWeapon = gunHand.GetComponent<Weapon>();
        health = GetComponent<Health>();
        playerState = GetComponent<PlayerStates>();
        networkPlayer = GetComponent<NetworkPlayer>();
        currentWeaponClip = currentWeapon.Clip;
        Clip.text = currentWeaponClip + "";
    }

    private void TimeManager_OnTick()
    {
        fireDelay -= (float)base.TimeManager.TickDelta;

        if(reloading)
            Clip.text = "Reloading " + fireDelay.ToString("0.0");

        if (reloading && fireDelay <= 0f)
        {
            currentWeaponClip = currentWeapon.Clip;
            Clip.text = currentWeaponClip + "";
            reloading = false;
        }

    }

    private void Update()
    {
        if (!base.IsOwner) return;

        HandleWeapon();

        if (playerState.state != State.Aiming)
        {
            consecutiveShot = 0;
            shootingLastFrame = false;
        }

        if (reloading) return;

        if (playerState.state == State.Aiming && _input.mainAction && fireDelay <= 0f)
        {
            Vector3 aimPoint = GetAimPoint();
            Vector3 forward = AddSpread((aimPoint - gunExitPoint.position).normalized, consecutiveShot);
            FireWeapon(base.TimeManager.GetPreciseTick(TickType.Tick), gunExitPoint.position, forward);
        }
        else if(playerState.state == State.Aiming && !_input.mainAction)
        {
            consecutiveShot = 0;
            if(shootingLastFrame)
            {
                fireDelay = currentWeapon.BaseSingleShootDelay;
                shootingLastFrame = false;
            }
        }

    }

    private void HandleWeapon()
    {
        if (!playerState.CanAim())
        {
            SetAimState(false);
            Aiming?.Invoke(false);
            return;
        }

        if (playerState.state == State.Aiming)
        {
            SetAimState(true);
            Aiming?.Invoke(true);
        }
        else
        {
            SetAimState(false);
            Aiming?.Invoke(false);
        }
    }

    [ServerRpc]
    public void ThrowBall(Vector3 startPos)
    {
        if (fireDelay > 0f) return;
        fireDelay = baseFireDelay;
        GameObject ball = Instantiate(ballPrefab, startPos, Quaternion.identity);
        base.Spawn(ball, null);
        ball.GetComponent<Rigidbody>().AddForce(transform.forward * 120f);
    }

    [ServerRpc(RunLocally = true)]
    private void FireWeapon(PreciseTick pt, Vector3 position, Vector3 forward)
    {
        if (fireDelay > 0f) return;
        fireDelay = currentWeapon.BaseAutoShootDelay;
        shootingLastFrame = true;
        consecutiveShot++;
        currentWeaponClip--;
        Clip.text = currentWeaponClip + "";

        if (currentWeaponClip <= 0)
        {
            fireDelay = currentWeapon.BaseReloadDelay;
            reloading = true;
            consecutiveShot = 0;
        }

        

        Ray shootingRay = new Ray(position, forward);
        bool enableRollback = base.IsServer;

        if (enableRollback)
            RollbackManager.Rollback(pt, FishNet.Component.ColliderRollback.RollbackManager.PhysicsType.ThreeDimensional);

        health.SetIgnoreRaycast(true);


        if (Physics.Raycast(shootingRay, out RaycastHit hit, float.PositiveInfinity, shootingLayer))
        {
            if (hit.collider.gameObject.CompareTag("Player"))
            {
                ShootingEffect(position, hit.point, hit.normal, HitTarget.ENEMY);
                ShootClient(position, hit.point, hit.normal, HitTarget.ENEMY);
                
            }
            else if(hit.collider.gameObject.CompareTag("InvisibleBorder"))
            {
                ShootingEffect(position, hit.point, hit.normal, HitTarget.NONE);
                ShootClient(position, hit.point, hit.normal, HitTarget.NONE);
            }
            else
            {
                ShootingEffect(position, hit.point, hit.normal, HitTarget.SURROUNDING);
                ShootClient(position, hit.point, hit.normal, HitTarget.SURROUNDING);
            }
            
            if (base.IsClient)
                if (hit.collider.gameObject.CompareTag("Enemy"))
                {
                    Debug.Log("Client hit Enemy");
                }

            //Apply damage and other server things.
            if (base.IsServer)
            {
                if (hit.collider.gameObject.CompareTag("Player"))
                {
                    hit.collider.gameObject.GetComponent<HurtBox>().Hit(currentWeapon.Damage, GetComponent<NetworkCharacter>(), currentWeapon.WeaponName);
                }
            }
        }

        health.SetIgnoreRaycast(false);

        if (enableRollback)
            RollbackManager.Return();


    }


    [ObserversRpc]
    private void ShootClient(Vector3 ShootPosition, Vector3 hitPosition, Vector3 hitNormal, HitTarget target)
    {
        if (!base.IsOwner)
        {
            ShootingEffect(ShootPosition, hitPosition, hitNormal, target);
        }
    }


    private void ShootingEffect(Vector3 ShootPosition, Vector3 hitPosition, Vector3 hitNormal, HitTarget target)
    {
        if (base.IsServerOnly) return;
        shootEffect.Play();
        GameObject r = Instantiate(muzzleEffect, gunExitPoint.position, gunExitPoint.rotation);
        r.transform.parent = gunExitPoint;

         var trail = Instantiate(bulletTrail, ShootPosition, Quaternion.LookRotation(hitPosition - gunExitPoint.position));
        trail.GetComponent<HitscanTrail>().SetEndPos(hitPosition, target, hitNormal);
    }

    private Vector3 GetAimPoint()
    {
        Vector2 centerScreenPoint = new Vector2(Screen.width / 2f, Screen.height / 2f);
        Ray ray = mainCamera.ScreenPointToRay(centerScreenPoint);
        if (Physics.Raycast(ray, out RaycastHit raycastHit, float.PositiveInfinity, shootingLayer))
        {
            return raycastHit.point;
        }
        else
        {
            return mainCamera.transform.position + mainCamera.transform.forward * 200f;
        }
    }

    private Vector3 AddSpread(Vector3 aimPoint, int consecutiveShot)
    {
        if (consecutiveShot < 2)
        {
            networkPlayer.AddCameraPitch(-0.1f);
            return aimPoint;
        }

        float spreadValue = currentWeapon.SpreadValue * (consecutiveShot < 10 ? consecutiveShot : 10);

        if(consecutiveShot< 10)
            networkPlayer.AddCameraPitch(-0.2f);
        else if(consecutiveShot < 20)
            networkPlayer.AddCameraPitch(-0.3f);
        else
            networkPlayer.AddCameraPitch(-0.4f);

        Vector3 newAimPoint = new Vector3(
            Random.Range(aimPoint.x - 0, aimPoint.x + 0),
            Random.Range(aimPoint.y - spreadValue, aimPoint.y + spreadValue),
            Random.Range(aimPoint.z - spreadValue, aimPoint.z + spreadValue));
        return newAimPoint;
    }


    [ServerRpc]
    public void SetAimState(bool state)
    {
        SetGunState(state);
    }

    [ObserversRpc(RunLocally = true)]
    public void SetGunState(bool aim)
    {
        gunBack.SetActive(!aim);
        gunHand.SetActive(aim);
    }


}
