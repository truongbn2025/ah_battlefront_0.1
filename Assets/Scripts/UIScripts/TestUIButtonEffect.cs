using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DG.Tweening;
public class TestUIButtonEffect : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler
{
    [SerializeField] private Ease easeType;
    private Color hightLightColor;
    private Color primitiveColor;
    private Sequence colorSequence;

    private void Awake()
    {
        primitiveColor = gameObject.GetComponent<Image>().color;
        hightLightColor = new Color(255, 0, 0);

    }

    private void ButtonEffect()
    {
        colorSequence = DOTween.Sequence();

        gameObject.GetComponent<RectTransform>().DOScale(1.15f, 0.25f).SetEase(easeType);
        colorSequence.Append(gameObject.GetComponent<Image>().DOColor(hightLightColor, 0.25f));
        colorSequence.AppendInterval(2);
        colorSequence.Append(gameObject.GetComponent<Image>().DOColor(primitiveColor, 0.75f).SetLoops(19, LoopType.Yoyo));
        //mySequence.OnComplete(() => mySequence.Append(gameObject.GetComponent<Image>().DOColor(primitiveColor, 0.25f)));
    }
    private void ExitButtonEffect()
    {
        colorSequence.Kill();
        gameObject.GetComponent<Image>().DOColor(primitiveColor, 0.25f);
        gameObject.GetComponent<RectTransform>().DOScale(1f, 0.25f).SetEase(easeType);
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        ButtonEffect();
    }
    public void OnPointerExit(PointerEventData eventData)
    {

        ExitButtonEffect();
    }
}
