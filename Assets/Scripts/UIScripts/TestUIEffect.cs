using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;



public class TestUIEffect : MonoBehaviour, IPointerEnterHandler, IPointerExitHandler, IPointerDownHandler
{
    private RectTransform _rectTransform;
    private void OnEnable()
    {
        ChangePivot();
    }
    public void OnPointerEnter(PointerEventData eventData)
    {
            
            ToolTip.Instance.ShowToolTip(transform.position, CheckPosition());
    }
    public void OnPointerExit(PointerEventData eventData)
    {

        ToolTip.Instance.HideTooltip();
        //ToolTip.Instance.tooltipAvailable = false;
    }

    private void ChangePivot()
    {
        switch (CheckPosition())
        {
            case SCREEN_POSITION.BOTTOM_LEFT:
                _rectTransform.pivot = new Vector2(1, 1);
                break;
            case SCREEN_POSITION.TOP_LEFT:
                _rectTransform.pivot = new Vector2(1, 0);
                break;
            case SCREEN_POSITION.BOTTOM_RIGHT:
                _rectTransform.pivot = new Vector2(0, 1);
                break;
            case SCREEN_POSITION.TOP_RIGHT:
                _rectTransform.pivot = new Vector2(0, 0);
                break;
        }
    }
    SCREEN_POSITION CheckPosition()
    {
        if ((transform.position.y) > Screen.height / 2.0f && transform.position.x > Screen.width / 2.0f)
            return SCREEN_POSITION.TOP_RIGHT;
        if (!(transform.position.y > Screen.height / 2.0f) && transform.position.x > Screen.width / 2.0f)
            return SCREEN_POSITION.BOTTOM_RIGHT;
        if ((transform.position.y) > Screen.height / 2.0f && !(transform.position.x > Screen.width / 2.0f))
            return SCREEN_POSITION.TOP_LEFT;
        return SCREEN_POSITION.BOTTOM_LEFT;
    }
    public void OnPointerDown(PointerEventData eventData)
    {
        ToolTip.Instance.HideTooltip();
        //ToolTip.Instance.tooltipAvailable = false;
    }

    #region MonoBehavior
    private void Awake()
    {
        _rectTransform = GetComponent<RectTransform>();
        

    }
    #endregion
}
