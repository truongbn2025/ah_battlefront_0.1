using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;
public class TestSocialButtonEffect : MonoBehaviour ,IPointerEnterHandler, IPointerExitHandler
{
    public enum ICON_BUTTON_LOBBY
    {
        EXPLORE,
        NOTIFICATION,
        MENU
    }
    [SerializeField] private ICON_BUTTON_LOBBY targetType;
    [SerializeField] private Ease easeType;
    private RectTransform targetIcon;
    


    private void Start()
    {
        targetIcon = transform.GetChild(0).GetComponent<RectTransform>();
    }

    public void OnPointerEnter(PointerEventData eventData)
    {
        ButtonEffect();
    }

    public void OnPointerExit(PointerEventData eventData)
    {
        ExitButtonEffect();
    }

    private void ButtonEffect()
    {
        targetIcon.DOScale(1.7f, 0.5f).SetEase(easeType);
        ClassifyTarget(targetType);
    }

    private void ClassifyTarget(ICON_BUTTON_LOBBY target)
    {
        switch (target){
            case ICON_BUTTON_LOBBY.EXPLORE:
                transform.GetChild(0).GetComponent<RectTransform>().DORotate(new Vector3(0, 0, 360), 1f, RotateMode.FastBeyond360);
                break;
            case ICON_BUTTON_LOBBY.NOTIFICATION:
                targetIcon.DORotate(new Vector3(0, 0, 17), 0.1f, RotateMode.Fast)
                    .OnComplete(() => targetIcon.DORotate(new Vector3(0, 0, -17), 0.1f, RotateMode.Fast))
                    .SetLoops(6, LoopType.Yoyo)
                    .OnComplete(() => targetIcon.DORotate(new Vector3(0, 0, 0), 0.2f, RotateMode.Fast));
                break;
            case ICON_BUTTON_LOBBY.MENU:
                break;
        }
    }

    private void ExitClassifyTarget(ICON_BUTTON_LOBBY target)
    {
        switch (target)
        {
            case ICON_BUTTON_LOBBY.EXPLORE:
                transform.GetChild(0).GetComponent<RectTransform>().DORotate(new Vector3(0, 0, -360), 1f, RotateMode.FastBeyond360);
                break;
            case ICON_BUTTON_LOBBY.NOTIFICATION:

                transform.GetChild(0).GetComponent<RectTransform>().DORotate(new Vector3(0, 0, 0), 0.2f, RotateMode.Fast);
                break;
            case ICON_BUTTON_LOBBY.MENU:
                break;
        }
    }

    private void ExitButtonEffect()
    {
        transform.GetChild(0).GetComponent<RectTransform>().DOScale(1f, 0.5f).SetEase(easeType);
        
        ExitClassifyTarget(targetType);
    }
}
