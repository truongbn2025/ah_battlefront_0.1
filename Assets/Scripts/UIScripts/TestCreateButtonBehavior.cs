using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using DG.Tweening;
public class TestCreateButtonBehavior : MonoBehaviour, IPointerDownHandler
{

    public Transform targetButtonPanel;
    public Transform hidingButtonPanel;

    private Transform playButton;
    private Transform joinButton;
    private Transform playOffline;

    private Vector3 hidingPositionPlayButton;
    private Vector3 hidingPositionJoinButton;
    private Vector3 hidingPositionPlayOfflineButton;

    private Vector3 targetPositionPlayButton;
    private Vector3 targetPositionJoinButton;
    private Vector3 targetPositionPlayOfflineButton;


    [SerializeField] private LOBBY_START_TYPE type;
    [SerializeField] private GameObject inputPanel;
    [SerializeField] private Transform hindingInputPanel;
    [SerializeField] private Ease easeType;
    [SerializeField] private TestPlayButtonBehavior playMenuButton;
    private Transform name;
    private Transform address;
    private Transform port;
    private Transform play;

    private Vector3 namePosition;
    private Vector3 addressPosition;
    private Vector3 portPosition;
    private Vector3 playPosition;

    private Vector3 initialInputPanelPosition;


    private bool inputPanelShowed;

    private void Start()
    {
        inputPanelShowed = false;
        inputPanel.SetActive(false);
        playButton = targetButtonPanel.GetChild(0);
        joinButton = targetButtonPanel.GetChild(1);
        playOffline = targetButtonPanel.GetChild(2);
        //assign target pos
        targetPositionPlayButton = playButton.GetComponent<RectTransform>().position;
        targetPositionJoinButton = joinButton.GetComponent<RectTransform>().position;
        targetPositionPlayOfflineButton = playOffline.GetComponent<RectTransform>().position;
        //hiding pos
        hidingPositionPlayButton = hidingButtonPanel.GetChild(0).GetComponent<RectTransform>().position;
        hidingPositionJoinButton = hidingButtonPanel.GetChild(1).GetComponent<RectTransform>().position;
        hidingPositionPlayOfflineButton = hidingButtonPanel.GetChild(2).GetComponent<RectTransform>().position;

        name = inputPanel.transform.GetChild(0);
        address = inputPanel.transform.GetChild(1);
        port = inputPanel.transform.GetChild(2);
        play = inputPanel.transform.GetChild(3);
        namePosition = name.GetComponent<RectTransform>().position;
        addressPosition = address.GetComponent<RectTransform>().position;
        portPosition = port.GetComponent<RectTransform>().position;
        playPosition = play.GetComponent<RectTransform>().position;
        //initialInputPanelPosition = inputPanel.GetComponent<RectTransform>().position;


    }

    private void Update()
    {
        if (!playMenuButton.displayed)
        {
            inputPanelShowed = false;
            inputPanel.SetActive(false);

        }

    }

    private void DisplayInputPanel()
    {
        inputPanel.SetActive(true);
        //inputPanel.GetComponent<RectTransform>().position = initialInputPanelPosition;
        name.GetComponent<RectTransform>().position = transform.position;
        address.GetComponent<RectTransform>().position = transform.position;
        port.GetComponent<RectTransform>().position = transform.position;
        play.GetComponent<RectTransform>().position = transform.position;

        name.DOMove(namePosition, 0.5f).SetEase(easeType);
        address.DOMove(addressPosition, 0.5f).SetEase(easeType);
        port.DOMove(portPosition, 0.5f).SetEase(easeType);
        play.DOMove(playPosition, 0.5f).SetEase(easeType);
    }

    private void ShowButton(LOBBY_START_TYPE type)
    {
        PlayerLobbyData.Instance.startType = type;
        Sequence mySequence;
        switch (type)
        {
            case LOBBY_START_TYPE.CREATE:
                mySequence = DOTween.Sequence();
                mySequence.Append(joinButton.DOMove(hidingPositionJoinButton, 0.5f).SetEase(easeType));
                mySequence.Join(playOffline.DOMove(hidingPositionPlayOfflineButton, 0.5f).SetEase(easeType));
                mySequence.OnComplete(() => DisplayInputPanel());
                break;
            case LOBBY_START_TYPE.JOIN:
                mySequence = DOTween.Sequence();
                mySequence.Append(playButton.DOMove(hidingPositionPlayButton, 0.5f).SetEase(easeType));
                mySequence.Join(playOffline.DOMove(hidingPositionPlayOfflineButton, 0.5f).SetEase(easeType));
                mySequence.Append(joinButton.DOMove(targetPositionPlayButton, 0.5f).SetEase(easeType));
                mySequence.OnComplete(() => DisplayInputPanel());
                break;
            case LOBBY_START_TYPE.LOCAL_DEBUG:
                mySequence = DOTween.Sequence();
                mySequence.Append(playButton.DOMove(hidingPositionPlayButton, 0.5f).SetEase(easeType));
                mySequence.Join(joinButton.DOMove(hidingPositionJoinButton, 0.5f).SetEase(easeType));
                mySequence.Append(playOffline.DOMove(targetPositionPlayButton, 0.5f).SetEase(easeType));
                mySequence.OnComplete(() => DisplayInputPanel());
                break;
        }
        

    }

    public void DisableInputField() { 
    }

    private void HideButton(LOBBY_START_TYPE type)
    {
        HideInputPanel();
     
    }

    private void HideInputPanel()
    {
        Sequence mySequence = DOTween.Sequence();
        mySequence.Append(name.DOMove(targetPositionPlayButton, 0.5f).SetEase(easeType));
        mySequence.Join(address.DOMove(targetPositionPlayButton, 0.5f).SetEase(easeType));
        mySequence.Join(port.DOMove(targetPositionPlayButton, 0.5f).SetEase(easeType));
        mySequence.Join(play.DOMove(targetPositionPlayButton, 0.5f).SetEase(easeType));
        mySequence.OnComplete(() => ShowButtonStartingPosition());
    }

    
    private void ShowButtonStartingPosition()
    {
        inputPanel.SetActive(false);
        playButton.DOMove(targetPositionPlayButton, 0.5f).SetEase(easeType);
        joinButton.DOMove(targetPositionJoinButton, 0.5f).SetEase(easeType);
        playOffline.DOMove(targetPositionPlayOfflineButton, 0.5f).SetEase(easeType);
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (inputPanelShowed)
            HideButton(type);
        else
            ShowButton(type);
        inputPanelShowed = !inputPanelShowed;

    }


}
