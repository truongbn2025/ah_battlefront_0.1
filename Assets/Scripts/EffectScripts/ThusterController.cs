using FishNet;
using FishNet.Object;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ThusterController : NetworkBehaviour
{
    private Animator animator;
    private NetworkCharacter networkCharacter;
    [SerializeField] private GameObject thruster;
    [SerializeField] private ParticleSystem engine1;
    [SerializeField] private ParticleSystem engine2;

    private void Awake()
    {
        animator = GetComponent<Animator>();
        networkCharacter = GetComponent<NetworkCharacter>();
        InstanceFinder.TimeManager.OnTick += OnTick;
    }

    public override void OnStartClient()
    {
        base.OnStartClient();
        //ClientDoneSpawning();
        
    }

    [ServerRpc(RunLocally = true)]
    private void ClientDoneSpawning()
    {
        
    }

    private void OnDestroy()
    {
        TimeManager.OnTick -= OnTick;
    }

    private void OnTick()
    {
        bool quickDive = animator.GetBool("QuickDive");
        bool forward = animator.GetFloat("DirectionZ") > 0.1f;

        if (networkCharacter.characterState == CharacterState.Playing)
        {
            Destroy(thruster);
            Destroy(this);
        }

        if(networkCharacter.characterState != CharacterState.Droping)
        {
            thruster.SetActive(false);
        }
        else
        {
            thruster.SetActive(true);
        }


        if (animator.GetBool("Grounded"))
        {
            engine1.Stop();
            engine2.Stop();
            return;
        }
        if (networkCharacter.characterState != CharacterState.Droping) return; 

        if (!forward && !quickDive)
        {
            engine1.Stop();
            engine2.Stop();
        }
        else if(quickDive)
        {
            engine1.Play();
            engine2.Play();
        }
        else if(forward)
        {
            //var main1 = engine1.main;
            //main1.startLifetimeMultiplier = 0.1f;
            engine1.Play();

            //var main2 = engine2.main;
            //main2.startLifetimeMultiplier = 0.1f;
            engine2.Play();
        }
    }
}
